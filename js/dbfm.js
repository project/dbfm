
/* namespace */
function dbFM() {}

/*
** Global variables
*/

// work to do - the browser window itself should be virtually the same now whether it's the main browser 
// window of the one that you call from the node screen. The ONLY difference should be the context 
// menus - so we needn't have 2 sets of code - lets simplify things. ZZZZZZZZZZ

//Translation possible by changing the array values (DO NOT ALTER KEYS!)
dbFM.js_msg = [];
dbFM.js_msg["mkdir"] = "Create New Dir";
dbFM.js_msg["file"] = "file";
dbFM.js_msg["directory"] = "directory";
dbFM.js_msg["work"] = "Working...";
dbFM.js_msg["refresh"] = "refresh";
dbFM.js_msg["sort"] = "sort by this column";
dbFM.js_msg["column1"] = "Name";
dbFM.js_msg["column2"] = "Modified";
dbFM.js_msg["column3"] = "Size";
dbFM.js_msg["attach-title"] = "Attached Files";
dbFM.js_msg["meta-title"] = "File Properties";    // changed the title
dbFM.js_msg["search-title"] = "File Search";
dbFM.js_msg["debug-title"] = "dbFM Debug";
dbFM.js_msg["search-cur"] = "Search current directory (see listing breadcrumb trail)";
dbFM.js_msg["no-match"] = "No match found";
dbFM.js_msg["search"] = "Search";
dbFM.js_msg["submit"] = "Submit";
dbFM.js_msg["clear"] = "Clear";
dbFM.js_msg["cancel"] = "Cancel";
dbFM.js_msg["replace"] = "Replace and Version original copy of ";
dbFM.js_msg["replace-del"] = "Replace and Delete original copy of ";
dbFM.js_msg["no-replace"] = "Version new copy of ";
dbFM.js_msg["cache"] = "Cache Content";
dbFM.js_msg["curdir-undef"] = "current directory undefined";
dbFM.js_msg["ajax-err"] = "server is unreachable";
dbFM.js_msg["list-err"] = "dir listing fetch fail";
dbFM.js_msg["mkdir-err"] = "mkdir fail";
dbFM.js_msg["move-err"] = "move operation fail";
dbFM.js_msg["attach-err"] = "attach fail";
dbFM.js_msg["fetch-att-err"] = "fetch attachments fail";
dbFM.js_msg["getmeta-err"] = "get metadata fail";
dbFM.js_msg["sendmeta-err"] = "submit metadata fail";
dbFM.js_msg["len-err"] = "Too long";
dbFM.js_msg["confirm-del0"] = "Do you want to delete the ";
dbFM.js_msg["confirm-del1"] = " and all its contents";
dbFM.js_msg["confirm-det"] = "Do you want to detach ";
dbFM.js_msg["confirm-dbrem0"] = "Do you want to remove ";
dbFM.js_msg["confirm-dbrem1"] = " from the database?\n     All properties and attachments for this file will be lost";
dbFM.js_msg["enum_local"] = "Do you want to add all files of this directory into the database?";
dbFM.js_msg["enum_recur"] = "Do you want to add all files of this directory and sub-directories into the database?";
dbFM.js_msg["file-dwld"] = "download file";
dbFM.js_msg["no-file-dwld"] = "file not in database";
dbFM.js_msg["inserted"] = " files inserted into db";
dbFM.js_msg["not-inserted"] = " files failed to insert into db";

dbFM.menu_msg = [];
dbFM.menu_msg["mkdir"] = "Create Subdirectory";
dbFM.menu_msg["rmdir"] = "Delete Directory";
dbFM.menu_msg["rm"] = "Delete File";
dbFM.menu_msg["rendir"] = "Rename Directory";
dbFM.menu_msg["ren"] = "Rename File";
dbFM.menu_msg["meta"] = "File Properties";
dbFM.menu_msg["att"] = "Create Link";    // no longer call it attach - now called link
dbFM.menu_msg["emb"] = "Embed Image";
dbFM.menu_msg["det"] = "Detach from Node"; // don't do this now?
dbFM.menu_msg["dwnld"] = "Download as file";
dbFM.menu_msg["enum"] = "Add file to database";
dbFM.menu_msg["denum"] = "Remove file from database";
//Do not translate any code below this line

dbFM.current = null;
dbFM.dropContainers = [];

dbFM.oldOnContextMenu = null;
dbFM.oldOnMouseDown = null;
dbFM.contextMenuDiv = null;
dbFM.eventListeners = [];

dbFM.browser = null;
dbFM.menu = null;
dbFM.dirTreeObj = null;
dbFM.dirListObj = null;
dbFM.attachObj = null;
dbFM.alrtObj = null;
dbFM.contextMenuObj = null;
dbFM.progressTObj = null;
dbFM.progressLObj = null;
dbFM.progressObj = null;
dbFM.leftSpaceObj = null;
dbFM.rightSpaceObj = null;
dbFM.filetitle = ' ';


// Start trapping EVERY mousemovement (the x and y posn)
//is this really necessary??
//if (document.layers) document.captureEvents(Event.MOUSEMOVE);
//document.onmousemove=mtrack;

dbFM.objHPop=null; // Our floating div
dbFM.posx=0; // Our mouseX
dbFM.posy=0; // Our mouseY
//should these offsets be variables? - why not hard code? - ah we need global variables rather than passing them?
dbFM.offsetX=16; // Offset X away from mouse
dbFM.offsetY=16; // Offset Y
dbFM.popUp = false; // Is it showing right now??!

dbFM.dbgObj = null;
dbFM.renameActive = null;

dbFM.dragCont = null;
dbFM.dragStart = null;
dbFM.dragging = false;
dbFM.activeCont = null;
dbFM.dropContainers = [];
dbFM.attachContainer = [];
dbFM.zindex = 10000;

//Name of node edit form hidden field where attached node list is populated by js
dbFM.attachFormInput = 'edit-attachlist' //ZZZZZdon't need this??

// This array determines which metadata fields are viewable/editable
// dbFM.db_table_keys must match the keys of the dbfm_file table
// -1 = hidden, 0 = read only, 256 = text, all other values is max char length
// the array returned by getTableKeys is populated by the server-side dbfm.module

//dbFM.db_table_keys = getTableKeys();


// a bit of helpful info:
// the way hover works is that you define listeners for the appropriate mouse events
// (mouseover, mouseout) these call a function which changes the class of the affected element
// adding '-selected' to the classname


dbFM.db_table_keys = {'fid':0, 'uid':-1, 'fpath':0, 'fname':255, 'fsize':0,
                       'fmime':0, 'ftitle':255, 'fdesc':256, 'fcreatedate':0,
                       'flang':16, 'fpublisher':255, 'fformat':255, 'fversion':0
                      };

dbFM.mime_type = { image_jpeg:"i", application_pdf:"pdf" };
dbFM.icons = {
   epdf:"pdf", ephp:"php", ephps:"php", ephp4:"php", ephp5:"php", eswf:"swf",
   esfa:"swf", exls:"xls", edoc:"doc", ertf:"doc", ezip:"zip", erar:"zip",
   egz:"zip", e7z:"zip", etxt:"doc", echm:"hlp", ehlp:"hlp", enfo:"nfo",
   expi:"xpi", ec:"c", eh:"h", emp3:"mp3", ewav:"mp3", esnd:"mp3", einc:"cod",
   esql:"sql", edbf:"sql", ediz:"nfo", eion:"nfo", emod:"mp3", es3m:"mp3",
   eavi:"avi", empg:"avi", empeg:"avi", ewma:"mp3", ewmv:"avi"
};

/*
** Functions to draw the dbfm browser
*/
if (Drupal.jsEnabled) {
  $(window).load(dbfmLayout);
//  $(window).load(dbfmAttachmentLayout);
  $(window).load(dbfmdragContainer);
  $(window).load(dbfmcontextContainer);
//  $(window).load(dbfmUploadAutoAttach);
  $(window).load(dbfmAttachPopupLayout);
}

/**
 * Determine browser
 */
dbFM.browserDetect = function() {
  this.isIE = false;
  var b = navigator.userAgent.toLowerCase();
  if (b.indexOf("msie") >= 0) {
    this.isIE = true;
  }
}

/**
 * Hashtable (used by directory listing cache)
 */
dbFM.ht = function() {
  this.flush();
}

dbFM.ht.prototype.put = function(key, value) {
  if((key == null) || (value == null)) {
    throw "NullPointerException {" + key + "},{" + value + "}";
  }
  this.hash[key] = value;
}

dbFM.ht.prototype.get = function(key) {
  return this.hash[key];
}

dbFM.ht.prototype.containsKey = function(key) {
  for(var i in this.hash) {
    if (i == key && this.hash[i] != null) {
      return true;
    }
  }
  return false;
}

dbFM.ht.prototype.remove = function(key) {
  if(this.hash[key]) {
    this.hash[key] = null;
    return true;
  }
  return false;
}

dbFM.ht.prototype.dump = function() {
  var values = new Array();
  for (var i in this.hash) {
    if (this.hash[i] != null)
    values.push(this.hash[i]);
  }
  return values;
}

dbFM.ht.prototype.flush = function() {
  this.hash = new Array();
}

/**
 * Hashable Hashtable (used by menus)
 */
dbFM.hht = function() {
  this.flush();
}

dbFM.hht.prototype.put = function(key, obj) {
  if((key == null) || (obj == null))
    throw "NullPointerException {" + key + "},{" + obj + "}";

  if(this.hash[key] == null) {
    this.keys[this.keys.length] = key;
    this.hash[key] = new Array();
 }

  // append new object to array
  this.hash[key].push(obj);
}

dbFM.hht.prototype.get = function(key) {
  return this.hash[key];
}

dbFM.hht.prototype.remove = function(key) {
  if(this.hash[key]) {
    this.hash[key] = null;
    return true;
  }
  return false;
}

dbFM.hht.prototype.removeSub = function(key, subkey) {
  if(this.hash[key][subkey]) {
    this.hash[key][subkey] = null;
    return true;
  }
  return false;
}

dbFM.hht.prototype.flush = function() {
  this.hash = new Array();
  this.keys = new Array();
}

/**
 * Create dbFM Manager at 'dbfm' id anchor. This is the main dbFM browser, as opposed to the browser 
 * loaded from the node / textareas when embedding / linking
 */
function dbfmLayout() {
  var layoutDiv = '';

  //does a 'get element by id' to find what we're pointing at
  layoutDiv = dbFM.$('dbfm');

  if (layoutDiv) {
    //checks what browser they're running
    dbFM.browser = new dbFM.browserDetect();

//ZZZZZZZtake out all the tests to see whether things are in the database

    //menu table is global to allow external functions to push new menu elements
    //into the menu array
    dbFM.menu = new dbFM.hht();
    try {
      dbFM.menu.put('root', new dbFM.menuElement(dbFM.menu_msg["mkdir"], dbFM.menuMkdir, ''));

      dbFM.menu.put('dir', new dbFM.menuElement(dbFM.menu_msg["mkdir"], dbFM.menuMkdir, ''));
      dbFM.menu.put('dir', new dbFM.menuElement(dbFM.menu_msg["rmdir"], dbFM.menuRemove, ''));
      dbFM.menu.put('dir', new dbFM.menuElement(dbFM.menu_msg["rendir"], dbFM.menuRename, ''));

      dbFM.menu.put('file', new dbFM.menuElement(dbFM.menu_msg["rm"], dbFM.menuRemove, ''));
      //rename has been taken out - replaced - due to the confusion (generated by Charly over filenames and filetitles
      dbFM.menu.put('file', new dbFM.menuElement(dbFM.menu_msg["ren"], dbFM.menuRename, ''));
      //menuFidVal is a kinda legacy thing that tests whether the objid starts with fid - if it does its in the database
      dbFM.menu.put('file', new dbFM.menuElement(dbFM.menu_msg["meta"], dbFM.menuGetmeta, dbFM.menuFidVal));
      dbFM.menu.put('file', new dbFM.menuElement(dbFM.menu_msg["dwnld"], dbFM.menuDownload, dbFM.menuFidVal));

      //decided to do this manually
      dbFM.menu.put('file', new dbFM.menuElement('Preview image', dbfmImagesPreview, dbfmImagesTest));
    } 
    catch(err) {
      alert("Menu Create err\n" + err);
    }

    var layout_cont = dbFM.ce('div');
    dbFM.alrtObj = new dbFM.alert(layout_cont, 'alertbox');
    var elTreeDiv = dbFM.ce('div');
    elTreeDiv.setAttribute('id', 'tree'); //css id
    layout_cont.appendChild(elTreeDiv);
    
    dbFM.dirTreeObj = new dbFM.tree(elTreeDiv, 'dirtree', 'Directory', 'readtree', dbFM.menu.get('root'), dbFM.menu.get('dir'));
    dbFM.progressObj = new dbFM.progress(layout_cont, 'progressT');
    //initialise the hover popup window- not currently used
    //initPop(layout_cont);
    //dbFM.objHPop = new dbFM.hoverPop(layout_cont, 'hoverpop');

    //lets put the spacers directly into layout this creates a div spacer with an id of leftspace
    dbFM.leftSpaceObj = new dbFM.spacer(layout_cont,'leftSpace');

    //lets put the spacers directly into layout
    dbFM.rightSpaceObj = new dbFM.spacer(layout_cont,'rightSpace');

    dbFM.dirListObj = new dbFM.list(layout_cont, 'dirlist', 'file', true, 'narrow', true, dbFM.menu.get('dir'), dbFM.menu.get('file'), true);
    dbFM.progressObj = new dbFM.progress(layout_cont, 'progressL');
    
    //progress bar then the property and search forms - I'm not going to add the progress bar until I add the tree and list
    //dbFM.progressTObj = new dbFM.progress(dirTreeObj, 'progressT');
    //dbFM.progressLObj = new dbFM.progress(dirListObj, 'progressL');

    //these will not be needed on the final layout (when everything's in an overlay window)
//    var metaObj = new dbFM.meta(layout_cont);
//    var searchObj = new dbFM.search(layout_cont, 'search');

    //insert trees, listing, search, metadata, progress and alert divs before upload fset
    layoutDiv.insertBefore(layout_cont, layoutDiv.firstChild);

    dbFM.contextMenuObj = new dbFM.context();

    //append debug div
    dbFM.dbgObj = new dbFM.debug(getDebugFlag() ? layoutDiv : '');

    dbFM.dirTreeObj.fetch(true);
  }
}

/**
 * Create Web File Manager in node-edit 'dbfm-inline' id anchor. ZZZZ note that the menu is much reduced - we only need 
 * a few options when linking or embedding
 */
 
//may not need a seperate layout functions in the future - hope to get everything using the same function.
function dbfmAttachPopupLayout () {
  var layoutDiv = '';
  layoutDiv = dbFM.$('dbfm-popup');
  if(layoutDiv) {
    dbfmLayoutFunc(layoutDiv);
  }
}


//used for the node form file browser
function dbfmAttachmentLayout() {
  var layoutDiv = '';
  layoutDiv = dbFM.$('dbfm-inline');
  if (layoutDiv) {
    dbfmLayoutFunc(layoutDiv);
  }
}


function dbfmLayoutFunc(layoutDiv) {

  //I'm surprised we have to do this - I'm sure every module will be looking this up - why isn't this a global??
  dbFM.browser = new dbFM.browserDetect();

  // I had a problem with the menu - having drawn the bits we ask it for it will then go on and try to attach the
  // extra image stuff ughhh!
  dbFM.menu = new dbFM.hht();
  try {
    dbFM.menu.put('file', new dbFM.menuElement(dbFM.menu_msg["att"], dbFM.menuAttach, dbFM.menuFidVal));
    //added a test to see whether it's an image file before embedding
    dbFM.menu.put('file', new dbFM.menuElement(dbFM.menu_msg["emb"], dbFM.menuEmbed, dbfmImagesTest));
    //decided to manually add the preview to the menu
    dbFM.menu.put('file', new dbFM.menuElement('Preview image', dbfmImagesPreview, dbfmImagesTest));

  } catch(err) {
    alert("Error Creating Menu\n" + err);
  }

  var layout_cont = dbFM.ce('div');
  dbFM.alrtObj = new dbFM.alert(layout_cont, 'alertbox');
  var elTreeDiv = dbFM.ce('div');
  elTreeDiv.setAttribute('id', 'tree'); //css id
  layout_cont.appendChild(elTreeDiv);

  dbFM.dirTreeObj = new dbFM.tree(elTreeDiv, 'dirtree', 'Directory', 'readtree', dbFM.menu.get('root'), dbFM.menu.get('dir'));
  dbFM.progressObj = new dbFM.progress(layout_cont, 'progress');
  dbFM.leftSpaceObj = new dbFM.spacer(layout_cont,'leftSpace');
  dbFM.rightSpaceObj = new dbFM.spacer(layout_cont,'rightSpace');
  dbFM.dirListObj = new dbFM.list(layout_cont, 'dirlist', 'node', true, 'narrow', true, dbFM.menu.get('dir'), dbFM.menu.get('file'), false);

  layoutDiv.insertBefore(layout_cont, layoutDiv.firstChild);

  dbFM.contextMenuObj = new dbFM.context();

  //append debug div ZZZ I tried just getting rid of this but then you get javascript errors
  dbFM.dbgObj = new dbFM.debug(getDebugFlag() ? layoutDiv : '');

  // new dbfm browser anchored to 'dbfm-attach' div in dbfm_form_alter()
  dbFM.attachObj = new dbFM.attach('dbfm-attach');
  dbFM.attachObj.fetch();
  dbFM.dirTreeObj.fetch(true);

}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
//in order for image preview to work with attachments I've found it easier to just move dbfm_image in here

// call the server to get info on the extra menus
function dbfmImagesGetMenusAjax() {

  var menus = '';
  var url = dbFM.ajaxUrl();
  var postObj = { action:"get resize menu items", filepath:''};
  dbFM.HTTPPost(url, dbfmImageExtender, menus, postObj);
  return false;
}

//ZZZZZZZZ don't think we use this now
// add the extra menu elements (if we pass dbfmImagesTest)
//I'm not sure we're going to do this - we only really need the preview menu so I'll just add it manually
function dbfmImageExtender(menus){
  menus=Drupal.parseJson(menus);
  var arLen=menus.length;
  for ( var i=0, len=arLen; i<len; ++i ){
    dbFM.menu.put('file', new dbFM.menuElement("Resize to "+menus[i], dbfmImagesResize, dbfmImageTestExtra));
  }

  dbFM.menu.put('file', new dbFM.menuElement('Preview image', dbfmImagesPreview, dbfmImagesTest));

}

//introduced to limit the context menus appearing for attachments - so what is Obj - the selected element
function dbfmImageTestExtra(obj) {
  var elDivdbFM;
  //tests whether we're on the attach screen
  if (elDivdbFM = document.getElementById('dbfm-inline')) {
    return false;
  }
  else {
    return dbfmImagesTest(obj);
  }
}

// we only get the extra menu elements for filetypes with extensions below
dbfmImagesTest = function(obj){

  switch(obj.ext.toLowerCase()){
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'bmp':
    case 'gif':
      return true;
    default:
      return false;
  }

}

//not sure I need this - what is obj??
dbfmImagesResize = function(obj){
  var url = dbFM.ajaxUrl();
  var path = obj.element.title;
  var postObj = { action:this.desc, filepath:encodeURIComponent(path)};
  dbFM.HTTPPost(url, dbfmImageResizeResult, obj, postObj);
  return false;
}
//not sure I need this
dbfmImageResizeResult = function(response){
  dbFM.dirListObj.refresh(dbFM.dirListObj.content.current);
  dbFM.alrtObj.msg();
  dbFM.alrtObj.msg(response);
}

// this positions a preview immdiatly under the dbfm browser - see dbfmImagePreviewDisplay
//what is the obj? is it the 
dbfmImagesPreview = function (obj) {
  var url = dbFM.ajaxUrl();
  obj.is_file = ((obj.element.className != 'dirrow') && (obj.element.className.substring(0,4) != 'tree'));
  var path = obj.element.title;

  //bloody stupid why not pass back the fid?? - ah! it's because there may be files that aren't in the database perhaps?
  dbFM.progressLObj.show();

  filepath = obj.element.title.split('/');
  dbFM.filetitle = filepath[filepath.length - 1];
  
  //this AJAX call returns the url for dbfm/send for the required file
  var postObj = { action:"preview image", filepath:encodeURIComponent(path)};

 //  dbFM.dbgObj.dbg("preview:", obj);
  dbFM.HTTPPost(url, dbfmImagePreviewDisplay, obj, postObj);
  dbFM.progressLObj.hide();
  return false;
}

function removeChildrenFromNode(node){
  if(node !== undefined && node !== null){
    var len = node.childNodes.length;
      while (node.hasChildNodes()){
        node.removeChild(node.firstChild);
      }
    }
    return;
}

// display the image just below the dbfm file browser
dbfmImagePreviewDisplay = function (response) {
  var elDiv;
  //if we can't find the preview div then lets make one
  if(!(elDiv = document.getElementById('preview-image'))){
    var elDiv = dbFM.ce('div');
    elDiv.setAttribute('id', 'preview-image');
  }
  else{
    removeChildrenFromNode(elDiv);
  }
  //we'll attach the title / filename just above the image
  //we have a problem here - the title isnt easy to get at because we're not passing the
  //file object through here - so I've made a global to get around this
  var elTitle = dbFM.ctn(dbFM.filetitle);
  var elImg = dbFM.ce('img');
  elImg.setAttribute('src', response);
  elDiv.appendChild(elTitle);
  elDiv.appendChild(elImg);
  
  //now we've got to glue the preview onto the bottom of the dbfm browser ...
  //unfortunatly, if we're in the attach window it's not called dbfm
   if (!(elDivdbFM = document.getElementById('tree'))){
    if(!(elDivdbFM = this.layoutDiv)){
      if(!(elDivdbFM = document.getElementById('dbfm'))){
        if(!(elDivdbFM = document.getElementById('dbfm-inline'))) {
          if(!(elDivdbFM = document.getElementById('dbfm-popup'))) return false;
        }
      }
    }
  }
  elDivdbFM.appendChild(elDiv);
  return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////



function dbfmdragContainer() {
  // Create the drag container that will show the item while dragging
  dbFM.dragCont = dbFM.ce('div');
  dbFM.dragCont.setAttribute('id','dragCont'); //id for css
  dbFM.dragCont.style.cssText = 'position:absolute;display:none;';
  document.body.appendChild(dbFM.dragCont);
}

function dbfmcontextContainer() {
  // Create the context menu container that will display the menu
  dbFM.oldOnContextMenu = document.body.oncontextmenu;
  dbFM.oldOnMouseDown = document.onmousedown;
  dbFM.contextMenuDiv = dbFM.ce('div');
  dbFM.contextMenuDiv.setAttribute('id', 'cxtCont');  //id for css
  dbFM.contextMenuDiv.style.cssText = 'position:absolute;display:none;';
  document.body.appendChild(dbFM.contextMenuDiv);
}

//////////////////////////////////////////////////////////////////////////////
// UPLOAD STUFF
//////////////////////////////////////////////////////////////////////////////


//Attaches the upload behaviour to the upload form.
function dbfmUploadAutoAttach(){
  $('input.dbfmupload').each(function () {
alert("This has all been disconnected now");
    var uri = this.value;
    var button = 'wfmatt-button';
    var wrapper = 'wfmatt-wrapper';
    var hide = 'wfmatt-hide';
    var upload = new dbFM.jsUpload(uri, button, wrapper, hide);
  });
}

/**
 * dbFM.jsUpload constructor
 *
 * 1st param is upload iframe url
 * 2nd param is id of redirected button object)
 * 3rd param is id of container of html repainted by ajax
 * 4th param is id of upload input field
 */
dbFM.jsUpload = function(uri, button, wrapper, hide) {
  // Note: these elements are replaced after an upload, so we re-select them
  // everytime they are needed.

  //we still need all this otherwise the upload / submit doesn't work
  this.button = '#'+ button;
  this.wrapper = '#'+ wrapper;
  this.hide = '#'+ hide;
  Drupal.redirectFormButton(uri, $(this.button).get(0), this);
}

//Handler for the form redirection submission.
dbFM.jsUpload.prototype.onsubmit = function () {
/*
//fails here - what's wrong with the progress fingy - the problem is that the progress bar isn't there because the 
//layout's different (non existant). Need a seperate css and js file just for uploads
//  dbFM.progressObj.show();
  var hide = this.hide;
alert("do we reach the upload onsubmit?");

  $(hide).fadeOut('slow');
*/
}

//Handler for the form redirection completion.
//this is all verycomplex and I need hardly any of it ZZZZZZZZZ needs revisiting
dbFM.jsUpload.prototype.oncomplete = function (data) {
  var dbfmPointer = document.getElementById('dbFM');

  dbfmPointer.dirListObj.refresh(dbfmPointer.dirListObj.content.current);

/*
//  var greything1;
//  var greything2;
alert("do we reach the upload oncomplete?");

  // Remove old form
  Drupal.freezeHeight(); // Avoid unnecessary scrolling
  $(this.wrapper).html('');

//do we still need to wrap this is an extra div?? WHERE do we fill in the directory??

  // Place HTML into temporary div
  var div = document.createElement('div');
  $(div).html(data['html']);
  $(this.wrapper).append(div);

  //this is where we redirect the submit button for the upload
  this.confirm = dbFM.$('replace-options');
  if(this.confirm) {
    var hide = this.hide;
    $(hide).fadeOut('slow');
    var cp = this;
    this.confirm.id = 'replace-options';
    var replaceButton = dbFM.ce('input');
    replaceButton.setAttribute('type', 'button');
    replaceButton.setAttribute('value', dbFM.js_msg["submit"]);
    var listener = dbFM.addEventListener(replaceButton, "click", function(e) { cp.submit(); dbFM.stopEvent(e); });
    this.confirm.appendChild(replaceButton);
    this.confirm.file = data['file'];
  } else {
    $(this.hide, div).fadeIn('slow');
    dbfmUploadAutoAttach();
  }

  Drupal.unfreezeHeight();
 // dbFM.progressObj.hide();

  //this should work ... but it doesn't ZZZZZZZZZZZ
  //I'm confused - this would be refreshing the directory list but surely it happens before we've processed the file result??
//  this.parent.dbFM.dirListObj.refresh();




  //now we need to clear the greybox NOTHING BLOODY WORKS!
//    $("#GB_window,#GB_overlay").hide();
//    this.opener.$("#GB_window,#GB_overlay").hide();
//document.getElementById('GB_window').display = 'none';

//document.getElementById('GB_overlay').display = 'none';
//greything1 = self.parent.document.getElementById('GB_window');
//greything1.hide();

//greything1 = self.parent.document.getElementById('GB_window');
//greything1.css({ display: 'none' });

//greything2 = self.parent.document.getElementById('GB_overlay');
//greything2.css({ display: 'none' });

//document.GB_hide();
//self.parent.document.GB_hide();
//alert("did this rujn");
   // this.hide();
   //this.parent.GB_hide()
  //window.opener.GB_hide();
  //this.parent.GB_hide();
  
  /// I'm really having trouble here - I can't seem to run the GB_hide function
  //strange because it's not part of a document - just a top level function
*/
}

//Handler for the form redirection error.  ZZZ does this work??
dbFM.jsUpload.prototype.onerror = function (error) {
  alert('An error occurred:\n\n'+ error);
  dbFM.progressLObj.hide();
  if(this.confirm)
    dbFM.clearNodeById('replace-options');
  // Undo hide
  $(this.hide).css({
    position: 'static',
    left: '0px'
  });
}

dbFM.jsUpload.prototype.submit = function () {
/*
alert("Do we get to the upload submit?");
  var inputs = [];
  inputs = this.confirm.getElementsByTagName('input');
  var selection = '';
  for(var i = 0; i < inputs.length; i++) {
    if(inputs[i].checked == true) {
      selection = i;
      break;
    }
  }
  if(typeof(selection) != 'undefined') {
    var url = dbFM.ajaxUrl();
    dbFM.progressObj.show();
    var postObj = { action:encodeURIComponent("version"), param0:encodeURIComponent(selection), param1:encodeURIComponent(this.confirm.file)};
    dbFM.HTTPPost(url, this.callback, this, postObj);
  }
*/
}

dbFM.jsUpload.prototype.callback = function(string, xmlhttp, cp) {
/*
alert("do we reach the upload callback?");
  dbFM.progressObj.hide();
  // Remove overwrite radios
  var parent = cp.confirm.parentNode;
  parent.removeChild(parent.firstChild);
  var hide = cp.hide;
  if (xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);

    if(result.status)
      dbFM.dirListObj.refresh();
    // Insert ajax feedback before upload form
    var elDiv = dbFM.ce('div');
    elDiv.className = 'upload-msg';
    elDiv.appendChild(dbFM.ctn(result.data));
    $(hide).before($(elDiv));

  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }

  // Undo hide
  $(hide).css({ display: 'block' });
  dbfmUploadAutoAttach();
  */
}
//ZZZZZZZZZZZcan surely get rid of the mkdir_flag parameter?
/**
 * dbFM.list constructor
 *
 * 2nd param is base id of listing (multiple listing objects must have unique ids)
 * 3rd param is base id of file rows of table
 * 4th param is flag to determine if table head has the 'Create New Dir' button
 * 5th param sets styling for listing
 * 6th param enables drag and drop
 * 7th param is directory menu array
 * 8th param is file menu array
 * 9th param is a flag to indicate whether this is the main browser window or an attach/popup window
 */
dbFM.list = function(parent, id, type, mkdir_flag, class_name, dd_enable, dir_menu, file_menu, is_main ) {
  var wl = this;
  this.id = id;
  this.type = type;
  this.dd_enable = dd_enable;
  this.dir_cxt_en = mkdir_flag; //if no create dir button, assume no dir cxt menu
  this.url = dbFM.ajaxUrl();
  this.sc_n = 0;
  this.sc_m = 0;
  this.sc_s = 0;
  this.content = '';
  this.iconDir = getIconDir();
  this.dir_menu = dir_menu;
  this.file_menu = file_menu;
  this.cache = new dbFM.ht();

  // ZZZZ why double quotes instead of single here?
  var node = dbFM.ce("div");
  node.setAttribute('id', this.id);
  node.className = class_name;
 
  var elTable = dbFM.ce('table');
  var elTableBody = dbFM.ce('tbody');
  elTableBody.setAttribute('id', this.id + 'body');

  // First Table Row (tr) - should we give this its own ID?
  var elTr = dbFM.ce('tr');
  elTr.setAttribute('id', this.id + 'firstrow');

  // Refresh Icon
  var elTd = dbFM.ce('td');
  elTd.className = 'navi';
  var elA = dbFM.ce('a');
  elA.setAttribute('href', '#');
  elA.setAttribute('title', dbFM.js_msg["refresh"]);
  var elImg = dbFM.ce('img');
  elImg.setAttribute('src', this.iconDir+ '/r.gif');
  elImg.setAttribute('alt', dbFM.js_msg["refresh"]);
  elA.appendChild(elImg);
  elTd.appendChild(elA);
  elTr.appendChild(elTd);          //make this a child of the first row
  var listener = dbFM.addEventListener(elA, "click", function(e) { wl.refresh(dbFM.current);dbFM.stopEvent(e); });

  // Breadcrumb trail
  var elTd = dbFM.ce('td');
  elTd.colSpan = (mkdir_flag === true) ? 2 : 3;
  elTd.setAttribute('class','navi');
  // Build breadcrumb trail inside span
  var elSpan = dbFM.ce('span');
  elSpan.setAttribute('id', this.id + 'bcrumb');
  elTd.appendChild(elSpan);
  elTr.appendChild(elTd);

  // the mkdir flag is ALWAYS set to true at the moment - maybe he planned to do something fancy in the future
  if(mkdir_flag === true) {
  // Create New Directory and allow db insertions
    var elTd = dbFM.ce('td');
    var elSpan = dbFM.ce('span');
    elSpan.id = 'buttons';

    var elA = dbFM.ce('a');
    elA.setAttribute('id', 'Search');
    elA.title = "Search Window";
    var elImg = dbFM.ce('img');
    elImg.setAttribute('alt', "Search");
    elImg.setAttribute('src', this.iconDir + '/search.gif');
    elA.appendChild(elImg);
    
    var listener = dbFM.addEventListener(elA, "click", function() { GB_show('search window','dbfm_searchit',250,400);});

    elSpan.appendChild(elA);

    //not available on the attach and popup screens
    if (is_main) {
      var elA = dbFM.ce('a');
      elA.title = "Create new folder";
      var elImg = dbFM.ce('img');
      elImg.setAttribute('alt', "New Folder");
      elImg.setAttribute('src', this.iconDir + '/dn.gif');
      elA.appendChild(elImg);
      var listener = dbFM.addEventListener(elA, "click", function(e) { dbFM.stopEvent(e);wl.mkdir(); });
      var listener = dbFM.addEventListener(elA, "mouseover", function() { this.className = 'button-selected'; });
      var listener = dbFM.addEventListener(elA, "mouseout", function() { this.className = ''; });
      elSpan.appendChild(elA);
    }
/* DON'T NEED A PROPERTIES BUTTON SINCE PROPERTY IS SPECIFIC TO AN INDIVIDUAL FILE
    //new functions to show file properties and launch the upload window
    var elA = dbFM.ce('a');
    elA.title = "Show / Edit File properties";
    var elImg = dbFM.ce('img');
    elImg.setAttribute('alt', "Show / Edit File properties");
    elImg.setAttribute('src', this.iconDir + '/properties.gif');
    elA.appendChild(elImg);
    var listener = dbFM.addEventListener(elA, "click", function() { GB_show('file properties window','dbfm_propertit',250,400);});


 //   var listener = dbFM.addEventListener(elA, "click", function(e) { dbFM.stopEvent(e);wl.properties(); });
    var listener = dbFM.addEventListener(elA, "mouseover", function() { this.className = 'button-selected'; });
    var listener = dbFM.addEventListener(elA, "mouseout", function() { this.className = ''; });
    elSpan.appendChild(elA);

*/
    var elA = dbFM.ce('a');
    elA.title = "Upload file to filesystem";
    var elImg = dbFM.ce('img');
    elImg.setAttribute('alt', "Upload file to filesystem");
    elImg.setAttribute('src', this.iconDir + '/file_upload.gif');
    elA.appendChild(elImg);
    //parameters are url, height and width
    var listener = dbFM.addEventListener(elA, "click", function() { GB_show('file upload window','dbfm_uploadit',250,400); });

//    var listener = dbFM.addEventListener(elA, "click", function(e) { dbFM.stopEvent(e);wl.fupload(); });
    var listener = dbFM.addEventListener(elA, "mouseover", function() { this.className = 'button-selected'; });
    var listener = dbFM.addEventListener(elA, "mouseout", function() { this.className = ''; });
    elSpan.appendChild(elA);

    var elA = dbFM.ce('a');
    elA.title = "Help";
    var elImg = dbFM.ce('img');
    elImg.setAttribute('alt', "Help");
    elImg.setAttribute('src', this.iconDir + '/hlp.gif');
    elA.appendChild(elImg);
    var listener = dbFM.addEventListener(elA, "click", function() { GB_show('Help window','dbfm_helpit',250,400);});

    var listener = dbFM.addEventListener(elA, "mouseover", function() { this.className = 'button-selected'; });
    var listener = dbFM.addEventListener(elA, "mouseout", function() { this.className = ''; });
    elSpan.appendChild(elA);

    elTd.appendChild(elSpan);
    elTr.appendChild(elTd);
  }

  elTableBody.appendChild(elTr);

  // Second Row
  var elTr = dbFM.ce('tr');
  elTr.setAttribute('id', this.id + 'head');

  // icon column
  var elTd = dbFM.ce('td');
  elTd.className = 'head';
  elTr.appendChild(elTd);

  // Sort dir/files column
  var elTd = dbFM.ce('td');
  elTd.className = 'head';
  var elA = dbFM.ce('a');
  elA.setAttribute('href', '#');
  elA.setAttribute('title', dbFM.js_msg["sort"]);
  
  //conditional on firstChild needed to prevent faults on world's worst browser
  var listener = dbFM.addEventListener(elA, "click", function(e) { wl.sc_n^=1;wl.loadList("n");if(this.firstChild)this.firstChild.src = wl.iconDir + '/' + ((wl.sc_n)?"up":"down") + '.gif'; dbFM.stopEvent(e); });

  var elImg = dbFM.ce('img');
  elImg.setAttribute('alt', dbFM.js_msg["sort"]);
  elImg.setAttribute('src', this.iconDir + '/down.gif');
  elA.appendChild(elImg);
  elA.appendChild(dbFM.ctn(dbFM.js_msg["column1"]));
  elTd.appendChild(elA);
  elTr.appendChild(elTd);

  // date/time column
  var elTd = dbFM.ce('td');
  elTd.className = 'head';
  var elA = dbFM.ce('a');
  elA.setAttribute('href', '#');
  var listener = dbFM.addEventListener(elA, "click", function(e) { wl.sc_m^=1;wl.loadList("m");if(this.firstChild)this.firstChild.src = wl.iconDir + '/' + ((wl.sc_m)?"up":"down") + '.gif';dbFM.stopEvent(e); });

  var elImg = dbFM.ce('img');
  elImg.setAttribute('alt', dbFM.js_msg["sort"]);
  elImg.setAttribute('src', this.iconDir + '/down.gif');
  elA.appendChild(elImg);
  elA.appendChild(dbFM.ctn(dbFM.js_msg["column2"]));
  elTd.appendChild(elA);
  elTr.appendChild(elTd);

  // size column
  var elTd = dbFM.ce('td');
  elTd.className = 'head';
  var elA = dbFM.ce('a');
  elA.setAttribute('href', '#');
  //another IE hack
  var listener = dbFM.addEventListener(elA, "click", function(e) { if(this.firstChild) {wl.sc_s^=1;wl.loadList("s"); this.firstChild.src = wl.iconDir + '/' + ((wl.sc_s)?"up":"down") + '.gif';} dbFM.stopEvent(e); });

  var elImg = dbFM.ce('img');
  elImg.setAttribute('alt', dbFM.js_msg["sort"]);
  elImg.setAttribute('src', this.iconDir + '/down.gif');
  elA.appendChild(elImg);
  elA.appendChild(dbFM.ctn(dbFM.js_msg["column3"]));
  elTd.appendChild(elA);
  elTr.appendChild(elTd);

  elTableBody.appendChild(elTr);
dbFM.progressLObj = new dbFM.progress(elTableBody, 'progressL'); //can we attach here??

  elTable.appendChild(elTableBody);
  node.appendChild(elTable);
  parent.appendChild(node);
}

dbFM.list.prototype.bcrumb = function() {
  var cp = this;
  dbFM.clearNodeById(this.id + 'bcrumb');
  elSpan = dbFM.$(this.id + 'bcrumb');
  var pth = [];
  for(var i = 0; i < this.content.bcrumb.length - 1; i++) {
    pth.push(this.content.bcrumb[i][1]);
    // display 'visible' portion of breadcrumb only
    if(this.content.bcrumb[i][0] == 'v') {
      elSpan.appendChild(dbFM.ctn(" / "));
      // No breadcrumb link necessary for current directory
      var elA = dbFM.ce('a');
      elA.setAttribute('href', '#');
      // join previous loop iterations to create path for title
      elA.setAttribute('title', pth.join("/"));
      elA.appendChild(dbFM.ctn(this.content.bcrumb[i][1]));
      //IE fix - click will change to the selected directory
      var listener = dbFM.addEventListener(elA, "click", function(e) { cp.selectBC(e);dbFM.stopEvent(e); });
      elSpan.appendChild(elA);
    }
  }
  if(this.content.bcrumb[i][0] == 'v') {
    elSpan.appendChild(dbFM.ctn(" / "));
    elSpan.appendChild(dbFM.ctn(this.content.bcrumb[i][1]));
  }
}

dbFM.list.prototype.selectBC = function(event) {
  var el = event.target||window.event.srcElement
  dbFM.selectDir(el.title);
}

dbFM.list.prototype.refresh = function(path) {
  if(path == null)
    path = dbFM.current;
  this.cache.remove(path);
  this.fetch(path);
}

//aha! so this runs whenever you change directory! and it loads dbFM.current into the uploadpath
//fetch takes a directory path as its parameter and calls the ajax 'read' function
dbFM.list.prototype.fetch = function(curr_dir) {
  dbFM.alrtObj.msg();
  if(curr_dir || (curr_dir = dbFM.current)) {
    dbFM.progressObj.show();

    //wipe out the preview pane, we'll put in an icon and appropriate text
    dbFM.filetitle = "No File Selected";
    dbfmImagePreviewDisplay(getIconDir() + '/no-preview.png');

    //update current dir if specific dir selected
    dbFM.current = curr_dir;
    dbFM.dbgObj.dbg('fetch: ', curr_dir);

    if(this.cache.containsKey(dbFM.current)) {
      dbFM.dbgObj.dbg('cache hit: ', dbFM.current);
      this.content = this.cache.get(dbFM.current);
      this.bcrumb();
      this.loadList();
      dbFM.progressObj.hide();
    } 
    else {
      //use th AJAX call to read to refresh the directory listings etc.
      var postObj = { action:"read", param0:encodeURIComponent(curr_dir) };
      dbFM.HTTPPost(this.url, this.callback, this, postObj);
    }
  } 
}


//ZZZZZZZZZthis doesn't get used when we have an upload popup - is it still needed YES!!!
dbFM.list.prototype.callback = function(string, xmlhttp, cp) {
  dbFM.progressObj.hide();
  if (xmlhttp.status == 200) {
    //content will now contain an array of stuff about the requested directory, breadcrumb, dirs and files
    cp.content = Drupal.parseJson(string);
    
    //after we've done a read we need to wipe out the preview pane, we'll put in an icon and appropriate text
    dbFM.filetitle = "No File Selected";
    dbfmImagePreviewDisplay(getIconDir() + '/no-preview.png');

    //debug
    dbFM.dbgObj.dbg("list fetch:", dbFM.dump(cp.content));

    if(cp.content.status) {
      cp.cache.put(cp.content.current, cp.content);
      cp.bcrumb();
      cp.loadList("n");
    } 
    else {
      dbFM.alrtObj.msg(dbFM.js_msg["list-err"]);
    }
  } 
  else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

// Function to create a new directory
dbFM.list.prototype.mkdir = function() {
  var dirname = mkdir_prompt();
  //if they've cancelled or left the name blank then give up
  if (dirname == "") {
    return;
  }
  dbFM.alrtObj.msg();
  dbFM.progressLObj.show();
  var postObj = { action:"mkdir", param0:encodeURIComponent(this.content.current), param1:dirname };
  dbFM.HTTPPost(this.url, this.mkdir_callback, this, postObj);
}

//this function will either return a valid directory name or the fact that the user's cancelled
//entering a blank string is equivalent to pressing cancel
function mkdir_prompt()
{
  //note that we can't test here to see whether the directory name is unique - that's still done at the server end
  var name=prompt("Enter the directory name: ","New_Folder");
  if (name==null)
    name = "";
  return (name);
}

//show the properties popup
dbFM.list.prototype.properties = function() {
  alert("Under construction - this will allow the user to view/edit the file properties");
}

dbFM.list.prototype.fupload = function() {
  alert("Under construction - this is the new way of launching the upload window");
}

dbFM.list.prototype.fhelp = function() {
  alert("Under construction - this will be the help popup");
}

//doesn't seem to use the result.data field to show returned error messages
dbFM.list.prototype.mkdir_callback = function(string, xmlhttp, cp) {
  dbFM.progressLObj.hide();
  if (xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);
    dbFM.dbgObj.dbg("mkdir=", result.status);
    dbFM.dbgObj.dbg("mkdir path=", result.data);

    if(result.status) {
      //it worked - lets do a rename now
      cp.refresh(cp.content.current);
      if(dbFM.dirTreeObj) {
        dbFM.dirTreeObj.fetch();
      }
    } 
    else {
      dbFM.alrtObj.msg(dbFM.js_msg["mkdir-err"]);
    }
  } 
  else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

//here's where we build the  file list

//sort the rows according to which column is selected
dbFM.list.prototype.loadList = function(sortcol) {
  this.c_dir = 0;
  this.c_fil = 0;

  var listHead = dbFM.$(this.id + 'head');
  var parent = listHead.parentNode;
  while(listHead.nextSibling)
    parent.removeChild(listHead.nextSibling);

  parent = dbFM.$(this.id + 'body');
  // Build directory rows and append to table
  if(this.content.dirs.length) {
    this.sortTable(this.content.dirs, sortcol);
    for(var i = 0; i < this.content.dirs.length; i++) {
      var dirrow = new dbFM.dirrow(parent, this.content.dirs[i], i, this.dd_enable, this.dir_cxt_en, this.dir_menu);
    }
  }
  // Build file rows and append to table
  // type determines file context menu - note that filerow now takes one less parameter content.db
  if(this.content.files.length) {
    this.sortTable(this.content.files, sortcol);
    for(var i = 0; i < this.content.files.length; i++) {
      var filerow = new dbFM.filerow(parent, this.content.files[i], this.type, i, this.dd_enable, this.file_menu);
    }
  }
}

dbFM.list.prototype.sortTable = function(arr, key) {
  switch (key) {
    case "s":
      arr.sort(dbFM.sortBySize);
      if(this.sc_s)
        arr.reverse();
      break;
    case "m":
      arr.sort(dbFM.sortByModified);
      if(this.sc_m)
        arr.reverse();
      break;
    case "n":
      arr.sort(dbFM.sortByName);
      if(this.sc_n)
        arr.reverse();
      break;
  }
}

/**
 * Table directory row object - here's where you add and change what's in your directory listing
 */
dbFM.dirrow = function(parent, dir, index, dd_enable, cxt_enable, menu) {
  var dr = this;
  this.iconDir = getIconDir();
  //id used for drop container
  var _id = 'dirlist' + index;
  var elTr = dbFM.ce('tr');
  this.element = elTr;
  this.element.className = 'dirrow';

  elTr.setAttribute('id', _id);
  elTr.setAttribute('title', dir.p);
  if(dd_enable)
    //dbFM.draggable must be created after title assigned to set current path
    this.dd = new dbFM.draggable(elTr, this.element.className);

  var elTd = dbFM.ce('td');
  var elImg = dbFM.ce('img');
  elImg.setAttribute('src', this.iconDir + '/d.gif');  // directory icon
  elImg.setAttribute('id', _id + 'dd');
  elImg.setAttribute('alt', 'Dir');
  this.menu = menu;
  elTd.appendChild(elImg);
  elTr.appendChild(elTd);

  var elTd = dbFM.ce('td');
  // Title of link = path
  this.clickObj = dbFM.ce('a');
  this.clickObj.setAttribute('href', '#');
  //title is path
  this.clickObj.setAttribute('title', dir.p);
  this.clickObj.appendChild(dbFM.ctn(dir.n));
  elTd.appendChild(this.clickObj);
  elTr.appendChild(elTd);

  var elTd = dbFM.ce('td');
  elTd.className = 'txt';
  elTd.appendChild(dbFM.ctn(dir.m));
  elTr.appendChild(elTd);

  var elTd = dbFM.ce('td');
  elTd.className = 'txt';
  if(dir.s) {
    var size = dbFM.size(dir.s);
    elTd.appendChild(dbFM.ctn(size));
  }
  elTr.appendChild(elTd);

  //mouse event listeners to execute the appropriate actions for a directory row ZZZZ directories will need properties
  //to show permissions - number of items etc.
  if(dd_enable) {
    var listener = dbFM.addEventListener(this.element, "mousedown", function(e) { dbFM.contextMenuObj.hideContextMenu(e); dr.select(e); });
  } 
  else {
    var listener = dbFM.addEventListener(this.element, "click", function(e) { dbFM.contextMenuObj.hideContextMenu(e); if(dbFM.renameActive == false)dbFM.selectDir(dr.element.title); });
  }
  var listener = dbFM.addEventListener(this.element, "mouseover", function() { dr.hover(dr.element, true); });
  var listener = dbFM.addEventListener(this.element, "mouseout", function() { dr.hover(dr.element, false); });
  if(cxt_enable) {
    var listener = dbFM.addEventListener(this.element, "contextmenu", function(e) { if(dbFM.renameActive == false)dbFM.contextMenuObj.showContextMenu(e, dr);dbFM.stopEvent(e); });
  }

  parent.appendChild(elTr);
  this.c_dir ++;
}
//the state is either true or false (depending on whether you're showing or hiding the pop-up hypertext) el is the element
dbFM.dirrow.prototype.hover = function(el, state) {
  if(state)
    el.className += '_selected';
  else {
    //remove the word selected - note this used to use a space
    el.className = el.className.split('_', 1);
  }
}

dbFM.dirrow.prototype.select = function(event) {
  var cp = this;
  if(dbFM.renameActive == true)
    return false;
  event = event || window.event;
  switch(event.target||event.srcElement) {
    case this.clickObj:
      // Determine mouse button
      var rightclick = dbFM.rclick(event);
      if(rightclick)
        break;
      setTimeout(function(){
        //if click then no dragging...
        if(dbFM.dragging==false) {
          dbFM.selectDir(cp.element.title);
        }
      },200);
      //passthrough
    default:
      this.dd.mouseButton(event);
      break;
  }
  return false;
}
//got rid of the 7th parameter which indicated whether or not the file was in the database
/**
 * dbFM.filerow constructor
 *
 * 1st param is parent obj
 * 2nd param is fileobject (see desc below)
 * 3rd param is base id name of file row
 * 4th param is unique number to append to id
 * 5th param enables drag and drop
 * 6th param is directory menu array
  *
 * fileobject elements:
 * id -> fid
 * n -> file name - NOW not ZZZZZZZZftitle
 * p -> path (doesn't include name)
 * s -> file size
 * m -> modified date
 * e -> mimetype extension
 * i -> image file.
 * w -> image width
 * h -> image height
 */
 //rewritten to show title instead of filename ZZZ then written back...
dbFM.filerow = function(parent, fileObj, idtype, index, dd_enable, file_menu) {
  var fr = this;
  this.iconDir = getIconDir();
  this.ext = fileObj.e;
  this.filepath = fileObj.p + '/' + fileObj.n;
  var elTr = dbFM.ce('tr');
  this.element = elTr;
  elTr.className = idtype + 'row';
  //drag object id used for download type
  if(typeof fileObj.id == "undefined") {
    this.row_id  = 'nodb' + index;
  } else {
    if(fileObj.id == 0)
      this.row_id = idtype + index;
    else
      this.row_id = 'fid' + fileObj.id;
  }
  elTr.setAttribute('id', this.row_id);

  //title of drag object is path for move
  elTr.setAttribute('title', this.filepath);
  if(dd_enable)
    this.dd = new dbFM.draggable(elTr, elTr.className);

  var elTd = dbFM.ce('td');
  var elImg = dbFM.ce('img');
  if(fileObj.i)
    elImg.setAttribute('src', this.iconDir + '/i.gif');
  else
    elImg.setAttribute('src', fr.getIconByExt());
  if((typeof fileObj.id == "undefined") || fileObj.id == 0) {
    // Make icon transparent if file not in db - TODO: fix for IE
    elImg.style.opacity = 0.5;
  }
  elImg.setAttribute('alt', 'File');
  this.menu = file_menu;
  elTd.appendChild(elImg);
  elTr.appendChild(elTd);

  //sets the clickobj to id if known, otherwise the file path 
  var elTd = dbFM.ce('td');
  this.clickObj = dbFM.ce('a');
  this.clickObj.setAttribute('href', '#');
  if((typeof fileObj.id == "undefined") || fileObj.id == 0)
    this.clickObj.setAttribute('title', this.filepath);
  else
    this.clickObj.setAttribute('title', fileObj.id);
  
  //uses the filename here
  this.clickObj.appendChild(dbFM.ctn(fileObj.n));
  elTd.appendChild(this.clickObj);
  elTr.appendChild(elTd);

  var elTd = dbFM.ce('td');
  elTd.className = 'txt';
  elTd.appendChild(dbFM.ctn(fileObj.m));
  elTr.appendChild(elTd);

  var elTd = dbFM.ce('td');
  elTd.className = 'txt';
  var size = dbFM.size(fileObj.s);
  elTd.appendChild(dbFM.ctn(size));
  elTr.appendChild(elTd);

  //mouse event listeners for a file row ZZZZ we need to add new listeners for a click on the ICON
  //and change the mouseoverfor the form element to show the properties
  if(dd_enable)
    var listener = dbFM.addEventListener(this.element, "mousedown", function(e) { dbFM.contextMenuObj.hideContextMenu(e); fr.select(e); });
  else
    var listener = dbFM.addEventListener(this.element, "click", function(e) { dbFM.contextMenuObj.hideContextMenu(e); if(dbFM.renameActive == false)dbFM.selectFile(fr.clickObj.title, fr.element); });
  var listener = dbFM.addEventListener(this.element, "mouseover", function() { fr.hover(fr.element, true); });
  var listener = dbFM.addEventListener(this.element, "mouseout", function() { fr.hover(fr.element, false); });
  var listener = dbFM.addEventListener(this.element, "contextmenu", function(e) { if(dbFM.renameActive == false)dbFM.contextMenuObj.showContextMenu(e, fr);dbFM.stopEvent(e); });


//  var listener = dbFM.addEventListener(this.element, "mouseover", function() { dbFM.hoverPop.show(fr.element, "hoverpop"); });
//  var listener = dbFM.addEventListener(this.element, "mouseout", function() { dbFM.hoverPop.hide(); });
//  var listener = dbFM.addEventListener(this.element, "mouseover", function() { doText('Hello World',fr.element); });
//  var listener = dbFM.addEventListener(this.element, "mouseout", function() { doClear(); });

  parent.appendChild(elTr);
  this.c_fil++;
}

// shows some file properties - modern browsers (like firefox, opera) support hover anyway. ie doesn't but can be persuaded to 
// see: http://www.xs4all.nl/~peterned/csshover.html
dbFM.filerow.prototype.hover = function(el, state) {
  if(state)
    el.className += '_selected';
  else
    el.className = el.className.split('_', 1);
}

dbFM.filerow.prototype.select = function(event) {
  var cp = this;
  if(dbFM.renameActive == true)
    return false;
  event = event || window.event;
  switch(event.target||event.srcElement) {
    case this.clickObj:
      // Determine mouse button
      var rightclick = dbFM.rclick(event);
      if(rightclick)
        break;
      //unusual - a function defined in the middle of another one
      setTimeout(function(){
        //if click then no dragging...
        if(dbFM.dragging == false) {
          //element id used for download method (dbfm_send||direct http access)
          dbFM.selectFile(cp.clickObj.title, cp.element);
        }
      },200);
      //passthrough
    default:
      this.dd.mouseButton(event);
      break;
  }
  return false;
}

dbFM.filerow.prototype.getIconByExt = function() {
  var icon = "";

  var ext = new String(this.ext);
  if(ext) {
    ext = ext.replace(/\//g, "_");
    if(dbFM.mime_type[ext]) {
      icon = this.iconDir + '/' + dbFM.mime_type[ext] + '.gif';
    } else if(dbFM.icons["e" + ext]) {
      icon = this.iconDir + '/' + dbFM.icons["e" + this.ext] + '.gif';
    }
  }

  // extension stored in file record of db fails - use pathname
  if(!(icon)) {
    ext = new String(this.filepath);
    var last = ext.lastIndexOf(".");
    if(last != -1)
      // "." found
      ext = ext.slice(last + 1);
    else
      ext = "";
    icon = this.iconDir + '/' + ((dbFM.icons["e" + ext]) ? dbFM.icons["e" + ext] : "f") + '.gif';
  }
  return icon;
}

/**
 * dbFM.tree constructor
 *
 * 2nd param is base id of listing (multiple tree objects must have unique ids)
 * 3rd param is name of tree
 * 4th param is ajax fetch operation
 * 5th param is root directory menu array
 * 6th param is non-root directory menu array
 */
dbFM.tree = function(parent, id, treeTitle, op, root_menu, dir_menu) {
  var wt = this;
  this.id = id;
  this.action = op;
  this.icondir = getIconDir();
  this.content = '';
  this.rootId = id + 'root';
  this.expAllData = [['collapse', 'minus', 'block'], ['expand', 'plus', 'none']];
  // Set tree exp/collapse behaviour on load (0 = expanded, 1 = collapsed)
  this.expAllIndex = 1;
  this.root_menu = root_menu;
  this.dir_menu = dir_menu;


  var node = dbFM.ce("div");
  node.setAttribute('id', id);

  //build tree and display
  var elA = dbFM.ce('a');
  dbFM.progressTObj = new dbFM.progress(elA, 'progressT');
  
  elA.setAttribute('href', '#');
  elA.setAttribute('title', dbFM.js_msg["refresh"]);
  var listener = dbFM.addEventListener(elA, "click", function(e) { wt.fetch();dbFM.stopEvent(e); });

  var elImg = dbFM.ce('img');
  elImg.setAttribute('src', this.icondir + '/r.gif');
  elImg.setAttribute('alt', dbFM.js_msg["refresh"]);
  elA.appendChild(elImg);
  node.appendChild(elA); 
  node.appendChild(dbFM.ctn(' ' + treeTitle + ' Tree'));

  // Expand/Collapse all folders buttons
  var elSpan = dbFM.ce("span");
  elSpan.setAttribute('id', id + 'exp');
  for(var i = 0; i < 2; i++) {
    var elA = dbFM.ce('a');
    elA.setAttribute('href', '#');
    if (i) {
      elA.setAttribute('title', 'expand tree');
      var listener = dbFM.addEventListener(elA, "click", function(e) { wt.exp(0);dbFM.stopEvent(e); });
    } else {
        elA.setAttribute('title', 'collapse tree');
        var listener = dbFM.addEventListener(elA, "click", function(e) { wt.exp(1);dbFM.stopEvent(e); });
    }
    var elImg = dbFM.ce('img');
    //fills the alt tabs for the tree?
    elImg.setAttribute('alt', this.expAllData[i][0]);
    elImg.setAttribute('src', this.icondir + '/' + this.expAllData[i][1] + '.gif');
    elA.appendChild(elImg);
    elSpan.appendChild(elA);
  }
  node.appendChild(elSpan);

  var rootDiv = dbFM.ce("div");
  rootDiv.setAttribute('id', this.rootId);

  node.appendChild(rootDiv);

  parent.appendChild(node);
}

dbFM.tree.prototype.fetch = function(list) {
  // Reset ul count (0=root)
  this.treeUlCounter = 0;
  this.treeNodeCounter = 0;
  this.list = list;

//where is the action set??
  var url = dbFM.ajaxUrl();
  dbFM.alrtObj.msg();
  //show the tree progress object
  dbFM.progressTObj.show();
//dbFM.dbgObj.dbg("tree fetch url:", url);

  //wipe out the preview pane, we'll put in an icon and appropriate text
  dbFM.filetitle = "No File Selected";
  dbfmImagePreviewDisplay(getIconDir() + '/no-preview.png');


  var postObj = { action:encodeURIComponent(this.action) };
  dbFM.HTTPPost(url, this.callback, this, postObj);
}

dbFM.tree.prototype.callback = function(string, xmlhttp, cp) {
  dbFM.progressTObj.hide();
  if (xmlhttp.status == 200) {
    cp.content = Drupal.parseJson(string);
dbFM.dbgObj.dbg("tree fetch:", dbFM.dump(cp.content));
    //clear dir_tree div
    dbFM.clearNodeById(cp.rootId);
    dbFM.dropContainers = [];

    // Recursively build directory tree from php associative array
    var parent = dbFM.$(cp.rootId);
    cp.buildTreeRecur(cp.content.tree, parent, '');
    cp.init();
    if(cp.list)
      dbFM.dirListObj.fetch(cp.content.current);
  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

dbFM.tree.prototype.getpath = function() {
  return this.content.current;
}

dbFM.tree.prototype.showHideNode = function (event) {
  event = event || window.event;
  var collapseIcon = event.target||event.srcElement;
  if(collapseIcon.style.visibility == 'hidden')
    return;
  var ul = collapseIcon.parentNode.parentNode.getElementsByTagName('ul')[0];
  if(collapseIcon.title == this.expAllData[0][0]) {
    collapseIcon.src = this.icondir + '/' + this.expAllData[1][1] + '.gif';
    collapseIcon.alt = this.expAllData[1][0];
    collapseIcon.title = this.expAllData[1][0];
    ul.style.display = this.expAllData[1][2];
  }else{
    collapseIcon.src = this.icondir + '/' + this.expAllData[0][1] + '.gif';
    collapseIcon.alt = this.expAllData[0][0];
    collapseIcon.title = this.expAllData[0][0];
    ul.style.display = this.expAllData[0][2];
  }
}

dbFM.tree.prototype.buildTreeRecur = function(content, parent, input_path) {
  var cp = this;
  //php associative array is always a single member array with nested objects
  //sometimes this test fails - do we know a better way of testing whether  this is an array?
  if(dbFM.isArray(content)) {
    this.buildTreeRecur(content[0], parent, '');
  } 
  else if (dbFM.isObject(content)) {
    if(input_path)
      input_path += "/";
    var elUl = dbFM.ce('ul');
    elUl.setAttribute('id', this.id + '_ul_' + this.treeUlCounter++);
    // sort object here since php does not have a case-sensitive ksort
    var sortDir = [];
    for (var i in content) {
      sortDir.push(i);
    }
    if(sortDir.length) {
      sortDir.sort(dbFM.sortByKey);
      for(var j = 0; j < sortDir.length; j++) {
        var newpath = input_path + sortDir[j];
        var elLi = dbFM.ce('li');
        var _id = this.id + 'node' + this.treeNodeCounter;
        elLi.setAttribute('id', _id);
        elLi.setAttribute('title', newpath);
        elLi.className = this.treeNodeCounter ? 'treenode':'treeroot';

        var elDiv = dbFM.ce('div');
        var elImg = dbFM.ce('img');
        elImg.setAttribute('src', this.icondir + '/' + this.expAllData[this.expAllIndex][1] + '.gif');
        elImg.setAttribute('alt', this.expAllData[this.expAllIndex][0]);
        elImg.setAttribute('title', this.expAllData[this.expAllIndex][0]);
        elDiv.appendChild(elImg);
        elLi.appendChild(elDiv);
        var listener = dbFM.addEventListener(elImg, "click", function(e) { cp.showHideNode(e);dbFM.stopEvent(e); });
        var treeNode = new dbFM.treeNode(elDiv, newpath, _id, elImg, this.treeNodeCounter ? this.dir_menu : this.root_menu);
        this.treeNodeCounter++;
        this.buildTreeRecur(content[sortDir[j]], elLi, newpath);
        elUl.appendChild(elLi);
      }
    }

    // Always show root
    if(elUl.id == this.id + '_ul_0')
      elUl.style.display='block';
    else
      elUl.style.display = this.expAllData[this.expAllIndex][2];

    parent.appendChild(elUl);
  }
}

dbFM.tree.prototype.init = function() {
  var rootDiv = dbFM.$(this.rootId);
  // Determine if expand/collapse icon is present
  // Create event objects for each element
  var dirItems = rootDiv.getElementsByTagName('li');
  if (dirItems.length) {
    for(var i = 0; i < dirItems.length; i++) {
      var subItems = dirItems[i].getElementsByTagName('ul');
      if(!subItems.length)
        // Hide collapse icon if no sub-directories
        dirItems[i].getElementsByTagName('img')[0].style.visibility='hidden';
    }
  }
}

dbFM.tree.prototype.exp = function(expcol) {
  dbFM.clearNodeById(this.rootId);
  var _tree = dbFM.$(this.rootId);

  // Rebuild dirtree ul/li objects from dirtree object
  this.expAllIndex = expcol;
  this.treeUlCounter = 0;
  this.treeNodeCounter = 0;

  this.buildTreeRecur(this.content.tree, _tree, '');
  this.init();
}

dbFM.treeNode = function(parent, path, id, expImg, menu) {
  var tn = this;
  this.parent = parent
  this.element = parent.parentNode;
  var icondir = getIconDir();
  this.expClickObj = expImg;
  var elImg = dbFM.ce('img');
  elImg.setAttribute('id', id + 'dd');
  elImg.setAttribute('src', icondir + '/d.gif');
  parent.appendChild(elImg);
  this.clickObj = dbFM.ce('a');
  this.clickObj.href = '#';
  //anchor title is path
  this.clickObj.setAttribute('title', path);
  if(this.element.className != 'treeroot') {
    // root isn't draggable
    this.dd = new dbFM.draggable(this.element, this.element.className);
  }
  var root = [];
  root = path.split('/');
  //textNode is last directory name of path string
  this.clickObj.appendChild(dbFM.ctn(root[root.length - 1]));
  parent.appendChild(this.clickObj);

  //mouse event listeners
  this.menu = menu;
  if(this.dd) {
  var listener = dbFM.addEventListener(parent, "mousedown", function(e) { dbFM.contextMenuObj.hideContextMenu(e);  tn.select(e); });
  } else {
    var listener = dbFM.addEventListener(parent, "click", function(e) { dbFM.selectDir(tn.clickObj.title); dbFM.stopEvent(e); });
  }
  var listener = dbFM.addEventListener(parent, "mouseover", function() { this.className = 'selected'; });
  var listener = dbFM.addEventListener(parent, "mouseout", function() { this.className = ''; });
  var listener = dbFM.addEventListener(parent, "contextmenu", function(e) { if(dbFM.renameActive == false)dbFM.contextMenuObj.showContextMenu(e, tn);dbFM.stopEvent(e); });
}

dbFM.treeNode.prototype.select = function(event) {
  var cp = this;
  // disable during rename
  if(dbFM.renameActive == true)
    return false;
  event = event|| window.event;
  switch(event.target||event.srcElement) {
    case this.expClickObj:
      break;
    case this.clickObj:
      // Determine mouse button
      var rightclick = dbFM.rclick(event);
      if(rightclick)
        // oncontextmenu will handle this
        break;
      setTimeout(function(){
        //if click then no dragging...
        if(dbFM.dragging==false) {
          dbFM.selectDir(cp.clickObj.title);
      }},200);
      //passthrough
    default:
      this.dd.mouseButton(event);
      break;
  }
  return false;
}

dbFM.selectDir = function(path) {
  dbFM.dirListObj.fetch(path);
}

//is this code correct? doesn't seem to put in the base URL for non-clean setups
dbFM.selectFile = function(path, el, as_file) {
  //files not in db use file path in tr title for download
  //files inside dbfm have 'fidxxx' as title
  var no_fid = false;
  if(el.id.substring(0,3) == 'fid') {
    path = 'dbfm_send/' + path;
    if(as_file == true)
      path += '/1';
  } else {
    no_fid = true;
    path = el.title
  }
  //window open is to open a new browser window rather than dumping everything in the existing window
  var cleanUrl = getCleanUrl();
  if(cleanUrl || no_fid == true) {
    var url = getBaseUrl();
//    window.location = url + '/' + path;
     window.open(url + '/' + path);
  } else {
//    window.location = '?q=' + path;
    window.open('?q=' + path);
  }
}

/**
 * dbFM.context constructor
 */
dbFM.context = function() {
  dbFM.renameActive = false;
}

dbFM.context.prototype.hideContextMenu = function(event) {
  // Hide context menu and restore document oncontextmenu handler
  dbFM.contextMenuDiv.style.display = 'none';
  document.body.oncontextmenu = dbFM.oldOnContextMenu;
  document.onmousedown = dbFM.oldOnMouseDown;
}

//obj is treenode|filerow|dirrow
dbFM.context.prototype.showContextMenu = function(event, obj) {
  var cp = this;
  this.element = obj.element;
  this.clickObj = obj.clickObj;
//dbFM.dbgObj.dbg('this.element.title:', this.element.title);
  document.body.oncontextmenu = new Function ("return false");
  event = event || window.event;
  var pos = Drupal.mousePosition(event);

  // Remove any existing list in our contextMenu.
  dbFM.clearNodeById('cxtCont');

  // Build menu ul and append to dbFM.contextMenuDiv
  if(obj.menu != null) {
    var elUl = dbFM.ce('ul');
    for(var j = 0; j < obj.menu.length; j++) {
      var elLi = dbFM.ce('li');
      //Run prepare function and return true if menuElement to appear in menu
      if(typeof(obj.menu[j].prepFunc) == 'function' && !obj.menu[j].prepFunc(obj)) {
        continue;
      }
      elLi.appendChild(dbFM.ctn(obj.menu[j].desc));
      //menu selection is title of event
      elLi.id = 'cxtMenu' + j;
      var listener = dbFM.addEventListener(elLi, "mousedown", function(e) { cp.selectMenu(e, obj); cp.hideContextMenu(e); dbFM.stopEvent(e); });
      var listener = dbFM.addEventListener(elLi, "mouseover", function(e) { cp.hover(e, true); });
      var listener = dbFM.addEventListener(elLi, "mouseout", function(e) { cp.hover(e, false); });
      elUl.appendChild(elLi);
    }
    dbFM.contextMenuDiv.appendChild(elUl);

    if(pos.x + dbFM.contextMenuDiv.offsetWidth > (document.documentElement.offsetWidth-20)){
      pos.x = pos.x + (document.documentElement.offsetWidth - (pos.x + dbFM.contextMenuDiv.offsetWidth)) - 20;
    }
    //display the context menu
    dbFM.contextMenuDiv.style.left = pos.x + 'px';
    dbFM.contextMenuDiv.style.top = pos.y + 'px';

    dbFM.contextMenuDiv.style.display ='block';
    dbFM.contextMenuDiv.style.zIndex = ++dbFM.zindex;
  }
  var listener = dbFM.addEventListener(document, "mousedown", function(e) { cp.hideContextMenu(e); });
  return false;
}

// IE does not understand 'this' inside event listener func
dbFM.context.prototype.hover = function(event, state) {
  var el = event.target||window.event.srcElement
  el.className = state ? 'cxtContHover' : '';
}

// Call dbFM.menuElement execute function
dbFM.context.prototype.selectMenu = function(event, obj) {
  event = event || window.event;
  obj.menu[(event.target||event.srcElement).id.substring(7,8)].execFunc(obj);
  return false;
}


dbFM.menuElement = function(desc, execFunc, prepFunc) {
  this.desc = desc;
  this.execFunc = execFunc;
  this.prepFunc = prepFunc;
}

//obj is treenode|filerow|dirrow
dbFM.menuElement.prototype.exec = function(obj) {
  this.execFunc(obj);
}

dbFM.menuRemove = function(obj) {
  var url = dbFM.ajaxUrl();
  obj.is_file = ((obj.element.className != 'dirrow') && (obj.element.className.substring(0,4) != 'tree'));
  var path = obj.element.title;
  obj.parentPath = path.substring(0, path.lastIndexOf("/"));
  if(dbFM.confirm(dbFM.js_msg["confirm-del0"] + (obj.is_file ? dbFM.js_msg["file"] : dbFM.js_msg["directory"]) + " " + path + (obj.is_file ? "" : dbFM.js_msg["confirm-del1"]) + "?")) {
    //not really sure which progress object to show here
    dbFM.progressLObj.show();
    var postObj = { action:"delete", param0:encodeURIComponent(path) };
    dbFM.HTTPPost(url, dbFM.ctx_callback, obj, postObj);
    return false;
  }
}

//this is mkdir as called from the context menu - not the most common way of doing it
dbFM.menuMkdir = function(obj) {
  var url = dbFM.ajaxUrl();
  obj.is_file = false;
  var path = obj.element.title;
  obj.parentPath = path;
  //now prompts for a directory name
  var dirname = mkdir_prompt();
  //if they've cancelled or left the name blank then give up
  if (dirname == "") {
    return;
  }
  dbFM.progressLObj.show();
  var postObj = { action:"mkdir", param0:encodeURIComponent(path), param1:dirname };
  dbFM.HTTPPost(url, dbFM.ctx_callback, obj, postObj);
}

//rename, on the other hand, is always called from the context menu and takes as parameter the object
dbFM.menuRename = function(obj) {

  obj.is_file = ((obj.element.className != 'dirrow') && (obj.element.className.substring(0,4) != 'tree'));
  obj.renInput = dbFM.ce('input');
  obj.renInput.setAttribute('type', 'textfield');
  obj.renInput.value = obj.clickObj.firstChild.nodeValue;
  //clone input element since dbFM.contextMenuObj isn't destroyed on DOM
  obj.tempInput = obj.renInput.cloneNode(true);
  obj.tempInput.setAttribute('autocomplete','off'); //FF focus bug
  obj.clickparent = obj.clickObj.parentNode;
  obj.clickparent.replaceChild(obj.tempInput, obj.clickObj);
  //no change (blur) - restore original textNode
  var listener = dbFM.addEventListener(obj.tempInput, "blur", function (e) { dbFM.renameActive = false; obj.clickparent.replaceChild(obj.clickObj, obj.tempInput);dbFM.stopEvent(e);  });
  //listen for enter key up - swap names (illegal names are ignored on server and next list
  //refresh will restore the proper filename)
  var listener = dbFM.addEventListener(obj.tempInput, "keydown", function(e) { if(dbFM.enter(e) && dbFM.renameActive == true){ dbFM.renameActive = false; dbFM.menuSwapname(obj); this.blur(); dbFM.stopEvent(e);} });
  setTimeout(function(){ obj.tempInput.focus();dbFM.renameActive = true; },10);
}

dbFM.menuSwapname = function(obj) {
  var url = dbFM.ajaxUrl();
  var oldpath = obj.element.title;
  obj.parentPath = oldpath.substring(0, oldpath.lastIndexOf("/"));
  var newpath = obj.parentPath + '/' + obj.tempInput.value;
  var postObj = { action:"rename", param0:encodeURIComponent(oldpath), param1:encodeURIComponent(newpath) };
  dbFM.HTTPPost(url, dbFM.ctx_callback, obj, postObj);
  return true;
}

//called by the context menu at present - seems to work 
dbFM.menuGetmeta = function(obj) {
  var url = dbFM.ajaxUrl();
//  dbFM.dbgObj.dbg("obj.clickObj.title:", obj.clickObj.title);
  dbFM.progressLObj.show();
  var postObj = { action:"getmeta", param0:encodeURIComponent(obj.clickObj.title) };
  dbFM.HTTPPost(url, dbFM.meta_callback, '', postObj);
}

// this is basically the same code as embed (below)
dbFM.menuAttach = function(obj) {
  var content = '';

  //build the content to be inserted into the node text - ZZNH
  content = "[dbfm|fid=" + obj.clickObj.title + "|alt=" + obj.element.title + "|title=" + obj.clickObj.textContent + "|type=attach]";
  dbfmToNodeText(content);
}

dbFM.menuEmbed = function(obj) {
  var content = '';

  //build the content to be inserted into the node text - ZZNH
  content = "[dbfm|fid=" + obj.clickObj.title + "|alt=" + obj.element.title + "|title=" + obj.clickObj.textContent + "|type=embed]";
  dbfmToNodeText(content);
}


dbFM.menuDetach = function(obj) {
  var path = obj.element.title;
  if(dbFM.confirm(dbFM.js_msg["confirm-det"] + path + "?")) {
    // update table
    obj.element.parentNode.removeChild(obj.element);
    // update form input
    var attach_arr = [];
    attach_arr = dbFM.$(dbFM.attachFormInput).value.split(',');
    var new_attach_arr = [];
    var j = 0;
    // tr elements use 'fid#' in 'title' field
    var fid = obj.element.id.substring(3);
    for(var i = 0; i < attach_arr.length; i++) {
      if(attach_arr[i] != fid) {
        new_attach_arr[j++] = attach_arr[i];
      }
    }
    dbFM.$(dbFM.attachFormInput).value = new_attach_arr.join(',');
  }
}

dbFM.menuDownload = function(obj) {
  dbFM.selectFile(obj.clickObj.title, obj.element, true);
}

dbFM.menuDbRem = function(obj) {
  var path = obj.element.title;
  if(dbFM.confirm(dbFM.js_msg["confirm-dbrem0"] + path + dbFM.js_msg["confirm-dbrem1"])) {
    var url = dbFM.ajaxUrl();
    dbFM.progressLObj.show();
    var postObj = { action:"dbrem", param0:encodeURIComponent(path) };
    // path for callback must be a directory - not a file
    path = path.substring(0, path.lastIndexOf("/"));
    dbFM.HTTPPost(url, dbFM.dbrem_callback, path, postObj);
  }
  return false;
}

dbFM.dbrem_callback = function(string, xmlhttp, path) {
  dbFM.alrtObj.msg();
  dbFM.progressLObj.hide();
  if (xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);
    dbFM.dbgObj.dbg("dbrem:", dbFM.dump(result));
    if (result.status) {
      dbFM.dirListObj.refresh(path);
    } else {
      if(result.data) {
        dbFM.alrtObj.msg(dbFM.dump(result.data, true));
      } else {
        dbFM.alrtObj.msg("operation fail");
      }
    }
  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

// insert - DON'T USE THIS WITH BLOBS - everything's already in the database
dbFM.menuInsert = function(obj) {
  dbFM.insert(obj.element.title, "file");
}
dbFM.insert = function(path, type) {
  var url = dbFM.ajaxUrl();
  dbFM.progressLObj.show();
  var postObj = { action:"insert", param0:encodeURIComponent(path), param1:type };
  if(type == "file") {
    // path for callback must be a directory - not a file
    path = path.substring(0, path.lastIndexOf("/"));
  }
  dbFM.HTTPPost(url, dbFM.insert_callback, path, postObj);
  return false;
}

dbFM.insert_callback = function(string, xmlhttp, path) {
  dbFM.alrtObj.msg();
  dbFM.progressLObj.hide();
  if (xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);
    dbFM.dbgObj.dbg("insert:", dbFM.dump(result));
    if (result.status) {
      //if success, update fetch
      if(result.data.cnt) {
        dbFM.dirListObj.refresh(path);
        dbFM.alrtObj.msg(result.data.cnt + dbFM.js_msg["inserted"]);
      }
      if(result.data.errcnt) {
        dbFM.alrtObj.msg(result.data.errcnt + dbFM.js_msg["not-inserted"]);
        dbFM.alrtObj.msg(dbFM.dump(result.data.err, true));
      }
    } else if(result.data.err) {
        dbFM.alrtObj.msg(dbFM.dump(result.data.err, true));
    } else {
      dbFM.alrtObj.msg("operation fail");
    }
  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

//menu prepFunc to determine if filerow has 'fid' as first 3 letters of id
dbFM.menuFidVal = function(obj) {
  if (obj.element.id.substring(0,3) == 'fid') {
    return true;
  }
  return false;
}

dbFM.menuNoFidVal = function(obj) {
  //dbFM area files not in db have '0' click object title
  if (obj.element.id.substring(0,3) != 'fid')
    return true;
  return false;
}

dbFM.confirm = function(text) {
  var agree = confirm(text);
  return agree ? true : false;
}

/**
 * Context menu callbacks
 */
 //having added a new file - I see a problem here ZZZZZZZZZ
 //we're using attachformInput - the form on the browser not the form in the popup
dbFM.attach_callback = function(string, xmlhttp, cp) {
  dbFM.progressLObj.hide();
  if (xmlhttp.status == 200) {
    //returns a single file record
    var result = Drupal.parseJson(string);
    //for some reason this debug fails and prevents attachments (fails testing arrays)
//    dbFM.dbgObj.dbg("attach", dbFM.dump(result));
    if (result.status) {
      var filerow = new dbFM.filerow(dbFM.$('dbfm-attachbody'), result.attach, 'attach', '', true, dbFM.menu.get('det'));
      var elInput = dbFM.$(dbFM.attachFormInput);
      elInput.setAttribute('value', (elInput.getAttribute('value')?elInput.getAttribute('value')+',':'') + result.attach.id);
    } 
    else
      dbFM.alrtObj.msg(dbFM.js_msg["attach-err"]);
  } 
  else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

//context menu callback function ZZZZ do I need these return values? I think not
dbFM.ctx_callback = function(string, xmlhttp, obj) {
  var retval = true;
  dbFM.progressLObj.hide();
  dbFM.alrtObj.msg();
  if (xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);
    dbFM.dbgObj.dbg("context:", dbFM.dump(result));
    dbFM.dbgObj.dbg("obj:", obj.droppath);
    if (result.status) {
      // if the ajax returned something sensible
      if(!obj.is_file) {
        //it's a directory 
        if(dbFM.dirTreeObj) {
          //refresh the directory tree
          dbFM.dirTreeObj.fetch();
        }
      }
      //if success, flush cache for updated fetch
      dbFM.dirListObj.refresh(obj.parentPath);
    } 
    else if(result.data) {
      dbFM.alrtObj.msg(dbFM.dump(result.data, true));
    } 
    else {
      dbFM.alrtObj.msg("operation fail");
      retval = false
    }
  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
    retval = false;
  }
  return retval;
}

// display the file properties - currently fills values onto a form

//somewhere in here it says THING has no properties - seems to be in Robs debug???
dbFM.meta_callback = function(string, xmlhttp, obj) {
  dbFM.progressLObj.hide();
  // if we've succesfully recieved valid data
  if (xmlhttp.status == 200) {
    var elDiv;
    //if we can't find a previous properties div then lets make one
    if(!(elDiv = document.getElementById('properties'))){
      var elDiv = dbFM.ce('div');
      elDiv.setAttribute('id', 'properties');
      leftProperty = new dbFM.spacer(elDiv,'leftSpace');
      rightProperty = new dbFM.spacer(elDiv,'rightSpace');
    }
    else{
      removeChildrenFromNode(elDiv);
    }
    var result = Drupal.parseJson(string);
    //I think this was erroring for some reason?
//ZZ    dbFM.dbgObj.dbg("meta result:", dbFM.dump(result));
    if(result.meta) {
//      var formDiv = dbFM.ce('div');
//      formDiv.setAttribute('id', 'form_div');

      //we've got some suitable data - now lets build a form - give the form an ID
      var elForm = dbFM.ce('form');
      var elTable = dbFM.ce('table');
      var elTBody = dbFM.ce('tbody');
      elForm.setAttribute('id', 'property-form');
      // steps through each entry in the meta table deciding whether  the field is a text field a read-only field  or a
      // hidden field and building a form accordingly
      for (var i in result.meta) {
        if(i == 'fid')
          // store the file id in a variable
          var fid = result.meta[i];

        if(dbFM.db_table_keys[i] === -1)
          // step past hidden fields
          continue;

        //need to add extra fields - change the metadata from the server so that we can give the fields decent names etc

        //build the table rows on each form
        var elTr = dbFM.ce('tr');
        var elTd = dbFM.ce('td');
        elTd.appendChild(dbFM.ctn(i + ': '));
        elTr.appendChild(elTd);

        var elTd = dbFM.ce('td');
        if(dbFM.db_table_keys[i] === 0) {
          // read only field
          elTd.appendChild(dbFM.ctn(result.meta[i]));
        } 
        else {
          // Modifiable field
          var elInput = dbFM.ce('input');
          elInput.setAttribute('type', 'textfield');
          elInput.setAttribute('name', i);
          elInput.setAttribute('size', '40');
          elInput.setAttribute('value', result.meta[i]);
          var listener = dbFM.addEventListener(elInput, "keyup", function(e) { if(dbFM.enter(e))dbFM.validateMetaInput(this); dbFM.stopEvent(e); });
          elTd.appendChild(elInput);
        }
        elTr.appendChild(elTd);
        // td for validation error msg
        var elTd = dbFM.ce('td');
        elTd.appendChild(dbFM.ctn(String.fromCharCode(160))); //&nbsp;
        elTr.appendChild(elTd);
        elTBody.appendChild(elTr);
      }  // thats all the fields added and the form complete except for the submit  and clear buttons

      var elTr = dbFM.ce('tr');
      var elTd = dbFM.ce('td');
      var button = dbFM.ce('input');
      button.setAttribute('type', 'button');
      button.setAttribute('value', dbFM.js_msg["submit"]);
      var listener = dbFM.addEventListener(button, "click", function(e) { dbFM.submitMeta(elForm, fid);dbFM.stopEvent(e);this.blur(); });

      elTd.appendChild(button);
      elTr.appendChild(elTd);

      var elTd = dbFM.ce('td');
      elTd.setAttribute('colspan', '2');
      var button = dbFM.ce('input');
      button.setAttribute('type', 'button');
      button.setAttribute('value', dbFM.js_msg["clear"]);
      //changing this to remove the form
      var listener = dbFM.addEventListener(button, "click", function(e) { dbFM.clearNodeById('meta-form');dbFM.stopEvent(e); });
      //ZZZZZZZZZZZZZZZZZZZZZZ
      elTd.appendChild(button);
      elTr.appendChild(elTd);

      elTBody.appendChild(elTr);
      elTable.appendChild(elTBody);
      elForm.appendChild(elTable);
//      formDiv.appendChild(elForm);
      elDiv.appendChild(elForm);

      //now we need to add the form to the end of the main layout for now
      if(!(elDivdbFM = this.layoutDiv)){
        if(!(elDivdbFM = document.getElementById('dbfm'))){
          if(!(elDivdbFM = document.getElementById('dbfm-inline'))) {
            if(!(elDivdbFM = document.getElementById('dbfm-popup'))) return false;
          }
        }
      }
      elDivdbFM.appendChild(elDiv);
 
    } else
      dbFM.alrtObj.msg(dbFM.js_msg["getmeta-err"]);
  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

// Validate the length of input
dbFM.validateMetaInput = function(field) {
  var len = dbFM.db_table_keys[field.name];
dbFM.dbgObj.dbg("validateMetaInput", field.name);
  if(len < 256) {
    var data = dbFM.trim(field.value);
    if(data.length > len) {
      var err_msg = dbFM.ctn(dbFM.js_msg["len-err"]);
      field.parentNode.nextSibling.style.color = 'red';
      field.parentNode.nextSibling.appendChild(err_msg);
      setTimeout(function(){field.focus();},10);
      return false;
    } else {
      while (field.parentNode.nextSibling.hasChildNodes())
        field.parentNode.nextSibling.removeChild(field.parentNode.nextSibling.firstChild);
    }
  }
  return true;
}

dbFM.submitMeta = function(form, fid) {
  var url = dbFM.ajaxUrl();
  var inputs = [];
  var metadata = [];
  inputs = form.getElementsByTagName('input');
  // Do not include submit + clear inputs
  var input_num = inputs.length - 2;
  for(var i = 0; i < input_num; i++) {
    if(!(dbFM.validateMetaInput(inputs[i])))
      break;
  }
  if(i == input_num) {
    for(var i = 0; i < input_num; i++) {
      metadata.push('#' + inputs[i].name + '=' + dbFM.trim(inputs[i].value));
    }
    dbFM.progressLObj.show();
    var postObj = { action:"putmeta", param0:encodeURIComponent(fid), param1:encodeURIComponent(metadata.join(",")) };
    dbFM.HTTPPost(url, dbFM.submitMetacallback, '', postObj);
  } else {
    alert('Correct Input');
  }
}

dbFM.submitMetacallback = function(string, xmlhttp, obj) {
  dbFM.progressLObj.hide();
  if (xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);
    if (result.status) {
      dbFM.dbgObj.dbg('meta result:', result.status);
    } else
      dbFM.alrtObj.msg(dbFM.js_msg["sendmeta-err"]);
  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

// Trim leading/trailing whitespace off string
dbFM.trim = function(str) {
  return str.replace(/^\s+|\s+$/g, '');
}

/*
 * dbFM.attach constructor
 * Load the attachments into node-edit
 * TO DO: retain contents of attachments until submit
 * 
 */
 dbFM.attach = function(parentId) {
  return false;
}
/*
 
dbFM.attach = function(parentId) {
  var wa = this;
  this.id = parentId;
  this.attached = '';
alert("We're trying to attach it to : " . parentId);
  var elTable = dbFM.ce('table');
  var elTableBody = dbFM.ce('tbody');
  //this div needed to append new attach rows
  elTableBody.setAttribute('id', this.id + 'body');

  //Header
  var elTr = dbFM.ce('tr');
  //this div is start of clearing nodes on fetch
  elTr.setAttribute('id', this.id + 'head');

  // icon column
  var elTd = dbFM.ce('td');
  elTd.className = 'head';
  elTr.appendChild(elTd);

  // Sort dir/files column
  var elTd = dbFM.ce('td');
  elTd.className = 'head';
  elTd.appendChild(dbFM.ctn(dbFM.js_msg["attach-title"]));
  elTr.appendChild(elTd);

  // date/time column
  var elTd = dbFM.ce('td');
  elTd.className = 'head';
  elTd.appendChild(dbFM.ctn(dbFM.js_msg["column2"]));
  elTr.appendChild(elTd);

  // file size column
  var elTd = dbFM.ce('td');
  elTd.className = 'head';
  elTd.appendChild(dbFM.ctn(dbFM.js_msg["column3"]));
  elTr.appendChild(elTd);

  elTableBody.appendChild(elTr);
  elTable.appendChild(elTableBody);
  dbFM.$(parentId).appendChild(elTable);

  // Nest metadata fieldset in attach fieldset ZZZZZZZZdon't need the metadata when attaching
  var metaObj = new dbFM.meta(dbFM.$(parentId));
}
*/

// called by the node edit form to return a list of files currently attached
dbFM.attach.prototype.fetch = function() {
//we're no longer using this
return;

  var url = dbFM.ajaxUrl();

  // action attribute of node-edit form contains the node number
  var node_url = dbFM.$('node-form').action;

  dbFM.progressObj.show();
  var postObj = { action:"attach", param0:encodeURIComponent(node_url) };
  dbFM.HTTPPost(url, this.callback, this, postObj);
}

dbFM.attach.prototype.callback = function(string, xmlhttp, obj) {
  dbFM.progressObj.hide();
  if (xmlhttp.status == 200) {
    result = Drupal.parseJson(string);
    if(result.status) {
      if(result.attach.length) {
        var elInput = dbFM.$(dbFM.attachFormInput);
        var elTableBody = dbFM.$(obj.id + 'body');
        for(var i = 0; i < result.attach.length; i++) {
          var filerow = new dbFM.filerow(elTableBody, result.attach[i], 'attach', '', true, dbFM.menu.get('det'));
          elInput.setAttribute('value', (elInput.getAttribute('value')?elInput.getAttribute('value')+',':'') + result.attach[i].id);
        }
      }
    } else
      dbFM.alrtObj.msg(dbFM.js_msg["fetch-att-err"]);
  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

/**
 * dbFM.meta constructor - builds the file properties form
 */
dbFM.meta = function(parent) {
  var elFset = dbFM.ce('fieldset');
  elFset.setAttribute('id', 'metadata');
  //is it collapsed? No but when you first draw it it has nothing in it
  dbFM.collapseFSet(elFset, dbFM.js_msg["meta-title"], false);  //ZZ
  var fsetWrapper = dbFM.ce('div');
  fsetWrapper.className = "fieldset-wrapper";
  var metadata_msg = dbFM.ce('div');
  metadata_msg.setAttribute('id', 'meta-form');
  fsetWrapper.appendChild(metadata_msg);
  elFset.appendChild(fsetWrapper);
  parent.appendChild(elFset);
}

/**
 * dbFM.search constructor. Allows them to enter a search value to search the current path for a file
 */
dbFM.search = function(parent, op) {
  var cp = this;
  this.iconDir = getIconDir();
  this.action = op;
  this.listId = 'searchresult';
  var elFset = dbFM.ce('fieldset');
  elFset.setAttribute('id', 'filesearch');
  dbFM.collapseFSet(elFset, dbFM.js_msg["search-title"], false);
  var fsetWrapper = dbFM.ce('div');
  fsetWrapper.className = "fieldset-wrapper";
  var elDiv = dbFM.ce('div');
  elDiv.className = "description";
  elDiv.appendChild(dbFM.ctn(dbFM.js_msg["search-cur"]));
  fsetWrapper.appendChild(elDiv);

  //give them somewhere for their search text
  var elInput = dbFM.ce('input');
  elInput.setAttribute('type', 'textfield');
  elInput.setAttribute('size', '40');
  fsetWrapper.appendChild(elInput);

  //and a button to press
  var searchButton = dbFM.ce('input');
  searchButton.setAttribute('type', 'button');
  searchButton.setAttribute('value', dbFM.js_msg["search"]);
  var listener = dbFM.addEventListener(searchButton, "click", function(e) { cp.submit(elInput.value);dbFM.stopEvent(e); });
  fsetWrapper.appendChild(searchButton);

  var elDiv = dbFM.ce('div');
  elDiv.setAttribute('id', this.listId);
  fsetWrapper.appendChild(elDiv);
  elFset.appendChild(fsetWrapper);
  parent.appendChild(elFset);
}

dbFM.search.prototype.submit = function(value) {
  var url = dbFM.ajaxUrl();
  dbFM.progressLObj.show();
  var postObj = { action:encodeURIComponent(this.action), param0:encodeURIComponent(dbFM.current), param1:encodeURIComponent(value) };
  dbFM.HTTPPost(url, this.callback, this, postObj);
}

dbFM.search.prototype.callback = function(string, xmlhttp, cp) {
  dbFM.progressLObj.hide();
  if (xmlhttp.status == 200) {
    var node = dbFM.$(cp.listId);
    // Empty directory listing node
    dbFM.clearNodeById(cp.listId);

    var result = Drupal.parseJson(string);

    if(result.files.length) {
      var searchList = dbFM.ce('ul');
      for(var i = (result.files.length - 1); i >= 0; i--) {
        var elLi = dbFM.ce('li');

        var elImg = dbFM.ce('img');
        elImg.setAttribute('src', cp.iconDir + '/d.gif');
        elImg.setAttribute('alt', 'Dir');
        //title is path to directory
        elImg.setAttribute('title', result.files[i].p);
        var listener = dbFM.addEventListener(elImg, "click", function() { dbFM.selectDir(this.title); });
        var listener = dbFM.addEventListener(elImg, "mouseover", function() { this.src = cp.iconDir + '/open.gif'; });
        var listener = dbFM.addEventListener(elImg, "mouseout", function() { this.src = cp.iconDir + '/d.gif'; });
        elLi.appendChild(elImg);

        elLi.appendChild(dbFM.ctn('  '));

        var elA = dbFM.ce('a');
        elA.setAttribute('href', '#');
        if(result.files[i].id) {
          //first three letters of id must be 'fid' for selectFile
          //tooltip indicates if file is downloadable
          elA.setAttribute('id', 'fidsrch' + result.files[i].id);
          elA.setAttribute('title', dbFM.js_msg["file-dwld"]);
          var listener = dbFM.addEventListener(elA, "click", function(e) { dbFM.selectFile(this.id.substring(7), this, true); dbFM.stopEvent(e); });
        } else {
          elA.setAttribute('title', dbFM.js_msg["no-file-dwld"]);
        }
        elA.appendChild(dbFM.ctn(result.files[i].n));
        elLi.appendChild(elA);

        searchList.appendChild(elLi);
        var listener = dbFM.addEventListener(elLi, "mouseover", function() { this.className = 'selected'; });
        var listener = dbFM.addEventListener(elLi, "mouseout", function() { this.className = ''; });
      }
      node.appendChild(searchList);
    } else {
      var no_match = dbFM.ctn(dbFM.js_msg["no-match"]);
      node.style.color = 'red';
      node.appendChild(no_match);
    }
  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

// dbFM debug Collapsible Fieldset
dbFM.debug = function(parent) {
  if(parent) {
    var elFset = dbFM.ce('fieldset');
    elFset.setAttribute('id', 'debug');
    dbFM.collapseFSet(elFset, dbFM.js_msg["debug-title"], false);
    var fsetWrapper = dbFM.ce('div');
    fsetWrapper.className = "fieldset-wrapper";
    var dbg_control = dbFM.ce('div');
    var elInput = dbFM.ce('input');
    elInput.setAttribute('type', 'button');
    elInput.setAttribute('value', dbFM.js_msg["clear"]);
    var listener = dbFM.addEventListener(elInput, "click", function(e) { dbFM.clearNodeById('dbg');dbFM.alrtObj.msg();dbFM.stopEvent(e); });
    dbg_control.appendChild(elInput);
    var elInput = dbFM.ce('input');
    elInput.setAttribute('type', 'button');
    elInput.setAttribute('value', dbFM.js_msg["cache"]);
    var listener = dbFM.addEventListener(elInput, "click", function(e) { dbFM.alrtObj.msg(dbFM.dump(dbFM.dirListObj.cache.dump()));dbFM.stopEvent(e); });
    dbg_control.appendChild(elInput);
    fsetWrapper.appendChild(dbg_control);
    this.dbg_cont = dbFM.ce('div');
    this.dbg_cont.setAttribute('id', 'dbg');
    fsetWrapper.appendChild(this.dbg_cont);
    elFset.appendChild(fsetWrapper);
    parent.appendChild(elFset);
  } else {
    this.dbg_cont = '';
  }
}

dbFM.debug.prototype.dbg = function(title, msg) {
  if(this.dbg_cont) {
    //put latest msg at top (less scrolling)
    elBr = dbFM.ce('br');
    this.dbg_cont.insertBefore(elBr, this.dbg_cont.firstChild);
    if(msg) {
      this.dbg_cont.insertBefore(dbFM.ctn(msg), this.dbg_cont.firstChild);
    }
    if(title) {
      var elSpan = dbFM.ce('span');
      elSpan.className = 'g';
      elSpan.appendChild(dbFM.ctn(title));
      this.dbg_cont.insertBefore(elSpan, this.dbg_cont.firstChild);
    }
  }
}

/**
 * Progress indicator
 */
dbFM.progress = function(parent, id) {
  this.iconDir = getIconDir();

  this.id = id;
  this.flag = false;
  var elSpan = dbFM.ce('span');
  elSpan.setAttribute('id', this.id);
  elSpan.className = 'progress';
  parent.appendChild(elSpan);
}


dbFM.progress.prototype.hide = function() {
  if(this.flag) {
    this.flag = false;
    dbFM.clearNodeById(this.id);
  }
}

//parameters are a text string and a background colour - we're scrapping the text string!
dbFM.progress.prototype.show = function(x, y) {
  if(!this.flag) {
    this.flag = true;
    var prog = dbFM.$(this.id);
    var elSpan = dbFM.ce('span');
    elSpan.style.backgroundColor = y;
    progImg = document.createElement('img');
    progImg.setAttribute('src', this.iconDir+ '/progress.gif');
//    elSpan.appendChild(dbFM.ctn(x));
    elSpan.appendChild(progImg);

    prog.appendChild(elSpan);
    prog.style.visibility = 'visible';
  }
}

/**
 * Alert indicator
 */
dbFM.alert = function(parent, id) {
  this.id = id;
  var elDiv = dbFM.ce('div');
  this.node = elDiv;
  elDiv.setAttribute('id', id);
  parent.appendChild(elDiv);
}

dbFM.alert.prototype.msg = function(msg) {
  if(!msg) {
    dbFM.clearNodeById(this.id);
  } else {
    var elSpan = dbFM.ce('span');
    elSpan.className = 'alertspan';
    elSpan.appendChild(dbFM.ctn(msg));
    this.node.appendChild(elSpan);
    this.lf();
  }
}

dbFM.alert.prototype.lf = function() {
  var elBr = dbFM.ce('br');
  this.node.appendChild(elBr);
}

/**
 * dbFM.draggable constructor
 */
dbFM.draggable = function(element, _class) {
  this.element = element;
  this.curpath = element.title;
  this.isTree = _class.substring(0,4) == 'tree';
  this.isDir = (_class == 'dirrow') || this.isTree;
  this.isAttach = _class == 'attachrow';
  this.icondir = getIconDir();
}

dbFM.draggable.prototype.mouseButton = function(event) {
  event = event || window.event;

  // Determine mouse button
  var rightclick = dbFM.rclick(event);
  if(!rightclick)
    this.beginDrag(event);
}

dbFM.draggable.prototype.beginDrag = function (event) {
  if (dbFM.dragging) {
    return;
  }
  dbFM.dragging = true;
  var cp = this;
  this.oldMoveHandler = document.onmousemove;
  document.onmousemove = function(e) { cp.handleDrag(e); };
  this.oldUpHandler = document.onmouseup;
  document.onmouseup = function(e) { cp.endDrag(e); };

  // Make transparent
  this.element.style.opacity = 0.5;

  // Process
  this.handleDrag(event);
}

dbFM.draggable.prototype.handleDrag = function (event) {
  if (!(dbFM.dragging)) {
    return;
  }
  var cp = this;
  event = event || window.event;

  //Build dbFM.dropContainers array
  //The dragStart flag ensures that the following executes once only at the
  //beginning of a drag-drop
  if (!(dbFM.dragStart)) {
    dbFM.dragStart = true;

    //copy dragged element into drag container and make visible
    this.offset = this.getObjectOffset(event, this.element);
    if(dbFM.browser.isIE && !this.isTree) {
      /* IE cannot clone table rows */
      var elDiv = dbFM.ce('div');
      elDiv.setAttribute('id', 'ieDragDiv');
      elDiv.appendChild(this.element.getElementsByTagName('img')[0].cloneNode(false));
      elDiv.appendChild(this.element.getElementsByTagName('a')[0].firstChild.cloneNode(false));
      dbFM.dragCont.appendChild(elDiv);
    } else {
      dbFM.dragCont.appendChild(this.element.cloneNode(true));
    }
    dbFM.dragCont.style.display = 'block'
    dbFM.dragCont.style.zIndex = ++dbFM.zindex;

    dbFM.dropContainers = [];

    // Build list drop container array
    if(!this.isAttach) {
      var dirListRows = dbFM.$('dirlist').getElementsByTagName('tr');
      var dirListCont = [];
      for(var k = 0; k < dirListRows.length; k++) {
        if (dirListRows[k].className == 'dirrow') {
          dirListCont.push(dirListRows[k]);
        }
      }
      if (dirListCont.length) {
        for(var i = 0; i < dirListCont.length; i++) {
          // DragDrop element is folder icon
          var droptarget = dirListCont[i];
          var cont_pos = this.objPosition(droptarget);
          var skip = false;
          var droppath = dirListCont[i].title;
          if(this.curpath == droppath)
            continue;
          if(this.isTree) {
            var curtemp = [];
            curtemp = this.curpath.split('/');
            var droptemp = [];
            droptemp = droppath.split('/');
            // Test if drop path is beneath the drag path
            for(var j = 0; j < curtemp.length; j++) {
              if(curtemp[j] != droptemp[j])
                break;
            }
            if(j == curtemp.length) {
              skip = true;
            } else {
              // Test if drop path is directly above drag path (already a subdir)
              for(var j = 0; j < droptemp.length; j++) {
                if(curtemp[j] != droptemp[j])
                 break;
              }
              if((j == droptemp.length) && (curtemp.length == j + 1))
                skip = true;
            }
          }
          if(skip == false) {
            // Valid drop container
            var container = { id: dirListCont[i].id, x: cont_pos.x, y: cont_pos.y, w: droptarget.offsetWidth, h: droptarget.offsetHeight };
            dbFM.dropContainers.push(container);
          }
        }
      }

      var dirTreeCont = '';
      dirTreeCont = dbFM.$("dirtree")
      if(dirTreeCont) {
          //reuse var for container list
          dirTreeCont = dbFM.$("dirtree").getElementsByTagName('li');
        if (dirTreeCont.length) {
          // Build tree drop container array
          for(var i = 0; i < dirTreeCont.length; i++) {
            // DragDrop element is folder icon
            var droptarget = dirTreeCont[i].getElementsByTagName('div')[0];
            var cont_pos = this.objPosition(droptarget);
            var skip = false;

            if(this.isTree) {
              // Prevent a directory drop onto itself or its direct parent li (already a sub-directory)
              if((dirTreeCont[i] != this.element) && (dirTreeCont[i] != this.element.parentNode.parentNode)) {
                var children = this.element.getElementsByTagName('li');
                // Prevent a directory drop into a sub-directory
                for(var j = 0; j < children.length; j++) {
                  if(children[j] == dirTreeCont[i])
                    skip = true;
                }
              } else {
                skip = true;
              }
            } else {
              var droppath = dirTreeCont[i].title;
              // A regex would be preferable here
              if(this.curpath != droppath){
                var curtemp = [];
                curtemp = this.curpath.split('/');
                var droptemp = [];
                droptemp = droppath.split('/');
                // Test if drop path is beneath the drag path
                for(var j = 0; j < curtemp.length; j++) {
                  if(curtemp[j] != droptemp[j])
                    break;
                }
                if(j == curtemp.length) {
                  skip = true;
                } else {
                  // Test if drop path is directly above drag path (already a subdir)
                  for(var j = 0; j < droptemp.length; j++) {
                    if(curtemp[j] != droptemp[j])
                      break;
                  }
                  if((j == droptemp.length) && (curtemp.length == j + 1))
                    skip = true;
                }
              } else {
                skip = true;
              }
            }

            if(skip == false) {
              // Valid drop container - add to array of drop containers
              var container = { id: dirTreeCont[i].id, x: cont_pos.x, y: cont_pos.y, w: droptarget.offsetWidth, h: droptarget.offsetHeight };
              dbFM.dropContainers.push(container);
            }
          }
        }
      }
    } else {
      // attachment container is attach table body
      var droptarget = dbFM.$('dbfm-attachbody');
      var cont_pos = this.objPosition(droptarget);
      dbFM.attachContainer = { x: cont_pos.x, y: cont_pos.y, w: droptarget.offsetWidth, h: droptarget.offsetHeight };
    }
  }

  var pos = Drupal.mousePosition(event);
  var x = pos.x - this.offset.x;
  var y = pos.y - this.offset.y;

  //Scroll page if near top or bottom edge.  Hardcoded values
  var scroll = this.scroll();
  if(typeof(window.innerHeight) == 'number') {
    if(pos.y > (window.innerHeight + scroll - 35)) {
      window.scrollBy(0,20);
    } else if(pos.y  - scroll < (25)) {
      window.scrollBy(0,-20);
    }
  } else if(document.documentElement && document.documentElement.clientHeight) {
    if(pos.y > (document.documentElement.clientHeight + scroll - 35)) {
      window.scrollBy(0,20);
    } else if(pos.y  - scroll < (25)) {
      window.scrollBy(0,-20);
    }
  }

  // move our drag container to wherever the mouse is (adjusted by mouseOffset)
  dbFM.dragCont.style.top  = y + 'px';
  dbFM.dragCont.style.left = x + 'px';

  if(!this.isAttach) {
    dbFM.activeCont = null;
    if(dbFM.dropContainers.length) {
      // Compare mouse position to every drop container
      for(var i = 0; i < dbFM.dropContainers.length; i++) {
        if((dbFM.dropContainers[i].x < pos.x) &&
           (dbFM.dropContainers[i].y < pos.y) &&
           ((dbFM.dropContainers[i].w + dbFM.dropContainers[i].x) > pos.x) &&
           ((dbFM.dropContainers[i].h + dbFM.dropContainers[i].y) > pos.y)) {
          // Found a valid drop container - Highlight selection
          dbFM.activeCont = dbFM.$(dbFM.dropContainers[i].id);
          dbFM.$(dbFM.dropContainers[i].id + 'dd').src = this.icondir + '/open.gif';
          dbFM.$(dbFM.dropContainers[i].id).className += '_selected';
        } else {
          // De-highlight container
          dbFM.$(dbFM.dropContainers[i].id + 'dd').src = this.icondir + '/d.gif';
          var class_names = [];
          class_names = dbFM.$(dbFM.dropContainers[i].id).className.split(' ');
          dbFM.$(dbFM.dropContainers[i].id).className = class_names[0];
        }
      }
    }
  } else {
    // Ignore all movement outside of the attach table
    if((dbFM.attachContainer.x < pos.x) &&
       (dbFM.attachContainer.y < pos.y) &&
       ((dbFM.attachContainer.w + dbFM.attachContainer.x) > pos.x) &&
       ((dbFM.attachContainer.h + dbFM.attachContainer.y) > pos.y)) {

      // In attach container
      var att_table_body = dbFM.$('dbfm-attachbody');
      var attachRows = att_table_body.getElementsByTagName('TR');
      var prevNode = '';
      var nextNode = '';
      var curr = false;
      // Start at 1 since first row is header
      for(var i = 1; i < attachRows.length; i++) {
        if(this.element.id != attachRows[i].id) {
          var att_pos = this.objPosition(attachRows[i]);
          if((att_pos.y + (attachRows[i].offsetHeight / 2)) < pos.y) {
            if(curr == true)
              prevNode = attachRows[i];
          }
          else if((att_pos.y + (attachRows[i].offsetHeight / 2)) > pos.y) {
            if(curr == false)
              nextNode = attachRows[i];
          }
        }
        else curr = true;
      }
      if(prevNode)
        att_table_body.insertBefore(prevNode, this.element);
      else if (nextNode)
        att_table_body.insertBefore(this.element, nextNode);
    }
  }

  //prevent selection of textnodes
  dbFM.stopEvent(event);
}

dbFM.draggable.prototype.endDrag = function (event) {
  if (!(dbFM.dragging)) {
    return;
  }
  event = event || window.event;

  // Uncapture mouse
  document.onmousemove = this.oldMoveHandler;
  document.onmouseup = this.oldUpHandler;

  // We remove anything that is in our drag container.
  dbFM.clearNodeById('dragCont');

  // Restore opacity
  this.element.style.opacity = 1.0;
  dbFM.dragging = false;
  dbFM.dragStart = false;
  dbFM.dragCont.style.display = 'none';
  dbFM.stopEvent(event);

  if(!this.isAttach) {
    // Move dragged object if a valid drop container
    if(dbFM.activeCont) {
      this.droppath = dbFM.activeCont.title;
      // De-highlight container
      dbFM.$(dbFM.activeCont.id + 'dd').src = this.icondir + '/d.gif';
      var class_names = [];
      class_names = dbFM.activeCont.className.split(' ');
      dbFM.activeCont.className = class_names[0];
      //prompt before we go any further
      if (confirm("Are you sure you want to move <" + this.curpath.substr(5) + "> to <" + this.droppath.substr(5) + "> ?")) {
        var url = dbFM.ajaxUrl();
        dbFM.progressLObj.show();
        var postObj = { action:"move", param0:encodeURIComponent(this.curpath), param1:encodeURIComponent(this.droppath) };
        dbFM.HTTPPost(url, this.callback, this, postObj);
      }
      dbFM.activeCont = null;
    }
  } else {
    // Put the current order of attachments into the form in preparation for submit
    var elInput = dbFM.$('edit-attachlist');
    var attachRows = dbFM.$('dbfm-attachbody').getElementsByTagName('tr');
    elInput.setAttribute('value', '');
    // Ignore 1st row (header)
    for(var i = 1; i < attachRows.length; i++) {
      var fid = attachRows[i].id.substring(3);
      elInput.setAttribute('value', (elInput.getAttribute('value')?elInput.getAttribute('value')+',':'') + fid);
    }
  }
}

dbFM.draggable.prototype.callback = function(string, xmlhttp, cp) {
  dbFM.progressLObj.hide();
  dbFM.alrtObj.msg();
  if(xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);
    dbFM.dbgObj.dbg("move result:", dbFM.dump(result));
    if(result.status) {
      //flush cache for target dir and source dir of object
      dbFM.dirListObj.cache.remove(cp.droppath);
      dbFM.dirListObj.cache.remove(cp.curpath.substring(0, cp.curpath.lastIndexOf("/")));
      //update tree if directory is moved
      if(cp.isDir)
        dbFM.dirTreeObj.fetch();
      if(!cp.isTree)
        //listing draggable - update current listing by removing dropped item
        cp.element.parentNode.removeChild(cp.element)
      else
        //tree draggable - update target directory
        dbFM.dirListObj.fetch(cp.droppath);
    } else {
      if(result.data)
        dbFM.alrtObj.msg(dbFM.dump(result.data, true));
      else
        dbFM.alrtObj.msg(dbFM.js_msg["move-err"]);
    }
  } else {
    dbFM.alrtObj.msg(dbFM.js_msg["ajax-err"]);
  }
}

dbFM.draggable.prototype.scroll = function () {
  var scrY = 0;
  if(typeof(window.pageYOffset) == 'number') {
    scrY = window.pageYOffset;
  } else if(document.body && document.body.scrollTop) {
    scrY = document.body.scrollTop;
  } else if(document.documentElement && document.documentElement.scrollTop) {
    scrY = document.documentElement.scrollTop;
  }
  return scrY;
}

dbFM.draggable.prototype.getObjectOffset = function(event, element) {
  var objPos = this.objPosition(element);
  var mousePos = Drupal.mousePosition(event);
  return {x:mousePos.x - objPos.x, y:mousePos.y - objPos.y};
}

dbFM.draggable.prototype.objPosition = function(element) {
  var curleft = curtop = 0;
  if(element.offsetParent) {
    curleft = element.offsetLeft;
    curtop = element.offsetTop;
    while(element = element.offsetParent) {
      curleft += element.offsetLeft;
      curtop += element.offsetTop;
    }
  }
  /* IE must compensate for relative positioning in css */
  if(dbFM.browser.isIE) {
    curleft -= 205;
    if(this.isTree)
      curleft -= 20;
  }
  return { x:curleft, y:curtop };
}

/**
 * Event Handler from http://ajaxcookbook.org (Creative Commons Attribution 2.5)
 */
dbFM.addEventListener = function(instance, eventName, listener) {
    var listenerFn = listener;
    if (instance.addEventListener) {
        instance.addEventListener(eventName, listenerFn, false);
    } else if (instance.attachEvent) {
        listenerFn = function() {
            listener(window.event);
        }
        instance.attachEvent("on" + eventName, listenerFn);
    } else {
        throw new Error("Event registration not supported");
    }
    var event = {
        instance: instance,
        name: eventName,
        listener: listenerFn
    };
    dbFM.eventListeners.push(event);
    return event;
}

dbFM.removeEventListener = function(event) {
    var instance = event.instance;
    if (instance.removeEventListener) {
        instance.removeEventListener(event.name, event.listener, false);
    } else if (instance.detachEvent) {
        instance.detachEvent("on" + event.name, event.listener);
    }
    for (var i = 0; i < dbFM.eventListeners.length; i++) {
      if (dbFM.eventListeners[i] == event) {
        dbFM.eventListeners.splice(i, 1);
        break;
      }
    }
}

//is there a problem here? Ahah! aparently I have to edit template.php because this
//doesn't clear up properly
dbFM.unregisterAllEvents = function() {
  while (dbFM.eventListeners.length > 0) {
    dbFM.removeEventListener(dbFM.eventListeners[0]);
  }
  dbFM.eventListeners = [];
}

/**
 * Helper Functions
 */
dbFM.$ = function(id) { return document.getElementById(id); }
dbFM.ctn = function(textNodeContents) { var textNode = document.createTextNode(textNodeContents); return textNode; }
dbFM.ce = function(elementName) { elementName = elementName.toLowerCase(); var element = document.createElement(elementName); return element; }

/**
 * Creates an HTTP POST request and sends the response to the callback function
 *
 * Note: passing null or undefined for 'object' makes the request fail in Opera 8.
 *       Pass an empty string instead.
 */
dbFM.HTTPPost = function(uri, callbackFunction, callbackParameter, object) {
  var xmlHttp = new XMLHttpRequest();
  var bAsync = true;
  if (!callbackFunction) {
    bAsync = false;
  }
  xmlHttp.open('POST', uri, bAsync);

  var toSend = '';
  if (typeof object == 'object') {
    xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    for (var i in object) {
      toSend += (toSend ? '&' : '') + i + '=' + encodeURIComponent(object[i]);
    }
  }
  else {
    toSend = object;
  }
  xmlHttp.send(toSend);

  if (bAsync) {
    xmlHttp.onreadystatechange = function() {
      if (xmlHttp.readyState == 4) {
        callbackFunction(xmlHttp.responseText, xmlHttp, callbackParameter);
      }
    }
    return xmlHttp;
  }
  else {
    return xmlHttp.responseText;
  }
}

/**
 * Prevents an event from propagating.
 */
dbFM.stopEvent = function(event) {
  if (event.preventDefault) {
    event.preventDefault();
    event.stopPropagation();
  }
  else {
    event.returnValue = false;
    event.cancelBubble = true;
  }
}

/**
 * Maintain compatibility with collapse.js
 * Since dbfm fieldsets are built directly on the DOM, collapse.js onload
 * event will not convert them automatically - we must invoke manually here
 * dontya just hate spaces in classnames??
 */
dbFM.collapseFSet = function(parent, text, collapse) {
  parent.className = 'collapsible collapsed';
  var elLegend = dbFM.ce('legend');
  parent.appendChild(elLegend);
  var fieldset = parent;
  // Expand if there are errors inside
  if (($('input.error, textarea.error, select.error', fieldset).size() > 0) || collapse) {
    parent.className = 'collapsible';
  }

  // Turn the legend into a clickable link and wrap the contents of the fieldset
  // in a div for easier animation
  $(elLegend).empty().append($('<a href="#">'+ text +'</a>').click(function() {
    var fieldset = $(elLegend).parents('fieldset:first')[0];
    // Don't animate multiple times
    if (!fieldset.animating) {
      fieldset.animating = true;
      Drupal.toggleFieldset(fieldset);
    }
    return false;
  }));
}


//the getModUrl() is used by contrib modules for custom ajax
dbFM.ajaxUrl = function() {
  var path = getBaseUrl() + "/?q=";
  return path += (typeof getModUrl == "undefined") ? "dbfm_js" : getModUrl();
}

// Empty all child nodes
dbFM.clearNodeById = function(elementId) {
  var node = dbFM.$(elementId);
  while (node.hasChildNodes())
    node.removeChild(node.firstChild);
}

dbFM.sortByName = function(a, b) {
  var x = a.n.toLowerCase();
  var y = b.n.toLowerCase();
  return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}
dbFM.sortBySize = function(a, b) {
  var x = parseInt(parseFloat(a.s));
  var y = parseInt(parseFloat(b.s));
  return x - y;
}
dbFM.sortByModified = function(a, b) {
  var x = a.m;
  var y = b.m;
  return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}
dbFM.sortByKey = function(a, b) {
  var x = a.toLowerCase();
  var y = b.toLowerCase();
  return ((x < y) ? -1 : ((x > y) ? 1 : 0));
}

dbFM.size = function(sz) {
  var size = sz;
  var units;
  if(size < 1024) units = " B";
  else {
    size = parseInt(size >> 10);
    if(size < 1024) units = " KB";
    else {
      size = parseInt(size >> 10);
      if(size < 1024) units = " MB";
      else {
        size = parseInt(size >> 10);
        if(size < 1024) units = " GB";
        else {
          size = parseInt(size >> 10);
          suffix = " TB";
        }
      }
    }
  }
  return size + units;
}

dbFM.enter = function(event) {
  event = event || window.event;
  var code;
  if (event.keyCode)
    code = event.keyCode;
  else if (e.which)
    code = e.which;
  return(code == 13) ? true : false;
}


dbFM.rclick= function(event) {
  if (event.which) {
    var rightclick = (event.which == 3);
  } else {
    if (event.button)
      var rightclick = (event.button == 2);
  }
  return rightclick;
}

// Dump debug function
// return a string version of a thing, without name.
// calls recursive function to actually traverse content.
dbFM.dump = function(content, txt_arr) {
  if(txt_arr == 'undefined')
    txt_arr = false;
  return dbFM.dumpRecur(content, 0, true, txt_arr) + "\n";
}

// recursive function traverses content, returns string version of thing
// content: what to dump.
// indent: how far to indent.
// neednl: true if need newline, false if not
dbFM.dumpRecur = function(content,indent,neednl,txt_arr) {
  var out = "";
  if (typeof(content) == 'function')
    return 'function';
  else if (dbFM.isArray(content)) {  // handle real arrays in brackets
    if (neednl) out += "\n"+dbFM.dumpSpaces(indent);
    if(!txt_arr) out+="[ ";
    var inside = false;
    for (var i=0; i<content.length; i++) {
      if (inside)
        out+=" \n"+dbFM.dumpSpaces(indent+1);
      else
        inside=true;
      out+=dbFM.dumpRecur(content[i],indent+1,false,txt_arr);
    }
    out+="\n"+dbFM.dumpSpaces(indent);
    if(!txt_arr) out+="]";
  } else if (dbFM.isObject(content)) {   // handle objects by association
    if (neednl) out+="\n"+dbFM.dumpSpaces(indent);
    if(!txt_arr) out+="{ ";
    var inside = false;
    for (var i in content) {
      if (inside)
        out+=" \n"+dbFM.dumpSpaces(indent+1);
      else
        inside = true;
      out+="'" + i + "':" + dbFM.dumpRecur(content[i],indent+1,true,txt_arr);
    }
    out+="\n"+dbFM.dumpSpaces(indent);
    if(!txt_arr) out+="}";
  } else if (typeof(content) == 'string') {
    out+="'" + content + "'";
  } else {
    out+=content;
  }
  return out;
}

// print n groups of two spaces for indent
dbFM.dumpSpaces = function(n) {
  var out = '';
  for (var i=0; i<n; i++) out += '  ';
  return out;
}

// Naive way to tell an array from an object:
// it is an array if it has a defined length
dbFM.isArray = function(thing) {
  if (typeof(thing) != 'object') return false;
  if (typeof(thing.length) == 'undefined') return false;
  return true;
}

// Naive way to tell an array from an object:
// it is an array if it has a defined length
dbFM.isObject = function(thing) {
  if (typeof(thing) != 'object') return false;
  if (typeof(thing.length) != 'undefined') return false;
  return true;
}


// this bungs a filter tag into the node editor window 
function dbfmToNodeText(content) {
  //test here for the presence of the tiny editor to determine whether we put 
  //the text straight into the text area
  var element = this.parent.window.document.getElementById('edit-body');
  var runningTiny = false;

  if (this.parent.tinyMCE.getEditorId(element.id) == null) {
    runningTiny = false;
  }
  else {
    runningTiny = true;
  }

  
  if (runningTiny) {
    this.parent.tinyMCE.execCommand("mceInsertContent", true, content);
    this.parent.tinyMCE.selectedInstance.repaint();

    // Close the dbfm window
    this.parent.GB_hide();
 //   this.parent.tinyMCEPopup.close();
    return false;
  }
  else {
    var textarea = dbFM.$('edit-body');
    var myDoc = window.document;
    var cursor;

    //test whether the textarea is on the current page or back on the parent 'opener' page (for popups)
    if (!(textarea)) {
      //the $ method didn't get it so
      textarea = this.parent.window.document.getElementById('edit-body');
      if (!(textarea)) {
        alert("An error has occurred - file cannot be linked / embedded");
        return;
      }
    }
    if (myDoc.selection) {
      textarea.focus();
      cursor = myDoc.selection.createRange();
      cursor.text = content;
    } 
    else if (textarea.selectionStart || textarea.selectionStart == "0") { /* Gecko-based engines: Mozilla, Camino, Firefox, Netscape */
      var startPos  = textarea.selectionStart;
      var endPos    = textarea.selectionEnd;
      var body      = textarea.value;
      textarea.value = body.substring(0, startPos) + content + body.substring(endPos, body.length);
    } 
    else { /* Worst case scenario: browsers that don't know about cursor position, Safari, OmniWeb, Konqueror */
      textarea.value +=content;  //this just adds the text at the end rather than inserting it at the cursor position
    }
    //we've done it, now close the browser overlay window
    this.parent.GB_hide();
  }
  //doesn't matter what we return
  return false;
}

/////////////////////////////////////////////////////////////////////////
///////////////extra functions added for the popup boxes ////////////////
/////////////////////////////////////////////////////////////////////////

// I don't need even a fraction of this! have to have a prune soon!

var currentMode;

function onChangeBrowseBy() {
  var formObj = frames['dbfm_header'].document.forms[0];
  browse = formObj['edit-browse'].value;
	frames['dbfm_main'].window.location.href = BASE_URL + 'index.php?q=dbfm/thumbs/' + browse;
}

function onClickUpload() {
//alert("On click upload"); doesn't seem to call this?? why NOT??
  frames['dbfm_main'].window.location.href = BASE_URL + 'index.php?q=dbfm/upload';
}

function onClickStartOver() {
  frames['dbfm_main'].window.location.href = BASE_URL + 'index.php?q=dbfm/thumbs/myimages';
}

function updateCaption() {
  var caption = frames['dbfm_main'].document.getElementById("caption");
  var title = frames['dbfm_main'].document.dbfm['edit-title'].value;
  var desc = frames['dbfm_main'].document.dbfm['edit-desc'].value;
  if (desc != '') {
    title = title + ': ';
  }
  caption.innerHTML = '<strong>' + title + '</strong>' + desc;
}

function onChangeHeight() {
  var formObj = frames['dbfm_main'].document.forms[0];
  var aspect = formObj['edit-aspect'].value;
  var height = formObj['edit-height'].value;
  formObj['edit-width'].value = Math.round(height * aspect);
}

function onChangeWidth() {
  var formObj = frames['dbfm_main'].document.forms[0];
  var aspect = formObj['edit-aspect'].value;
  var width = formObj['edit-width'].value;
  formObj['edit-height'].value = Math.round(width / aspect);
}

function onChangeLink() {
  var formObj = frames['dbfm_main'].document.forms[0];
  if (formObj['edit-link-options-visible'].value == 1) {
    if (formObj['edit-link'].value == 'url') {
      showElement('edit-url', 'inline');
    } 
    else {
      hideElement('edit-url');
    }
  }
}

function onChangeSizeLabel() {
  var formObj = frames['dbfm_main'].document.forms[0];
  if (formObj['edit-size-label'].value == 'other') {
    showElement('size-other', 'inline');
  } else {
    hideElement('size-other');
    //showElement('size-other', 'inline'); // uncomment for testing
    // get the new width and height
    var size = formObj['edit-size-label'].value.split('x');
    // this array is probably a bounding box size, not an actual image
    // size, so now we use the known aspect ratio to find the actual size
    var aspect = formObj['edit-aspect'].value;
    var width = size[0];
    var height = size[1];
    if (Math.round(width / aspect) <= height) { // width is controlling factor
      height = Math.round(width / aspect);
    } else { // height is controlling factor
      width = Math.round(height * aspect);
    }
    // fill the hidden width and height textboxes with these values
    formObj['edit-width'].value = width;
    formObj['edit-height'].value = height;
  }
}

//not sure about this - why does it need a global? can you do static in javascript?? No, you DO need a global.
function setHeader(mode) {
  if (currentMode != mode) {
    frames['dbfm_header'].window.location.href = BASE_URL + 'index.php?q=dbfm/header/' + mode;
  }
  currentMode = mode;
}

function showElement(id, format) {
  var docObj = frames['dbfm_main'].document;
  format = (format) ? format : 'block';
  if (docObj.layers) {
    docObj.layers[id].display = format;
  } else if (docObj.all) {
    docObj.all[id].style.display = format;
  } else if (docObj.getElementById) {
    docObj.getElementById(id).style.display = format;
  }
}

function hideElement(id) {
  var docObj = frames['dbfm_main'].document;
  if (docObj.layers) {
    docObj.layers[id].display = 'none';
  } else if (docObj.all) {
    docObj.all[id].style.display = 'none';
  } else if (docObj.getElementById) {
    docObj.getElementById(id).style.display = 'none';
  }
}

//This uses window opener - nicked from Andres code
function insertImage() {
  if (window.opener) {
    // Get variables from the fields on the properties frame
    var formObj = frames['dbfm_main'].document.forms[0];
    // Get mode	(see dbfm.module for detailed comments)
    if (formObj['edit-insertmode'].value == 'html') { // return so the page can submit normally and generate the HTML code
      return true;
    }
    else if (formObj['edit-insertmode'].value == 'html2') { 
      // HTML step 2 (processed code, ready to be inserted)
      var content = getHTML(formObj);
    }
    else {
      //this is the REAL thing, generates a drupal filter tag
      var content = getFilterTag(formObj);
    }
    insertToEditor(content);
    return false;
  } 
  else {
    alert('The image cannot be inserted because the parent window cannot be found.');
    return false;
  }
}

//called by the icon on the node form to display a greybox overlay window
//I've now added 2 extra parameters so that the boxes can be different sizes
function popit(url, gwidth, gheight) {
  if (gwidth == null) gwidth = 400;
  if (gheight == null) gheight = 600;
  GB_show('dB File Manager', url, gwidth, gheight);
  return false;
}

function getHTML(formObj) {
  var html = frames['dbfm_main'].document.getElementById("finalhtmlcode").innerHTML;
  return html;
}

//just adds a spacer element - a div
dbFM.spacer = function(parent,id) {
  this.id = id;
  var elDiv = dbFM.ce('div');
  elDiv.setAttribute('id', this.id);
  elDiv.className = 'spacer';
  
  
  
  progImg = document.createElement('img');
  progImg.setAttribute('src', getIconDir()+ '/spacer.png');
  progImg.setAttribute('width', '0px');
  progImg.setAttribute('height', '0px');
  elDiv.appendChild(progImg);
  parent.appendChild(elDiv);
}
/*
  //note that putting a space (or several) doesn't work
//  x = "does this add itself?";
  var prog = dbFM.$(this.id);

  x = "&nbsp;";
  x = &nbsp;
  elDiv.appendChild(dbFM.ctn(x));
  prog.appendChild(elDiv);
  prog.style.visibility = 'hidden';
  
  
  
  */
/*
  //parameters are a text string and a background colour NOT ANY MORE, the background colour can be defined in the css and the text is derived from an AJAX call
dbFM.spacer.prototype.show = function() {
  var prog = dbFM.$(this.id);
  var elDiv = dbFM.ce('div');
  elDiv.style.backgroundColor = y;
  x = "Here is some verbiage instead of a spacer";
  elDiv.appendChild(dbFM.ctn(x));

  prog.appendChild(elDiv);
  prog.style.visibility = 'hidden';
}

*/

/**
 * hover-over hypertext popup - passed the element it's associated with (it uses an AJAX call to provide content)
 * and the id of the hover object
 */
/* not yet used
dbFM.hoverPop = function(parent, id) {
  this.id = id;
  this.flag = false;
  var elSpan = dbFM.ce('span');
  elSpan.setAttribute('id', this.id);
  elSpan.className = 'hoverPop';
  parent.appendChild(elSpan);
}


dbFM.hoverPop.prototype.hide = function() {
  dbFM.clearNodeById(this.id);
}

//parameters are a text string and a background colour NOT ANY MORE, the background colour can be defined in the css and the text is derived from an AJAX call. The position and size can be derived from the calling object and the ammount we have to display
dbFM.hoverPop.prototype.show = function() {
  var prog = dbFM.$(this.id);
  var elSpan = dbFM.ce('span');
  elSpan.style.backgroundColor = y;
  x = "Here is some verbiage - we'll soon put the properties in";
  elSpan.appendChild(dbFM.ctn(x));

  prog.appendChild(elSpan);
  prog.style.visibility = 'visible';
}
/*
//the hoverpop  takes a single parameter - the element being hovered over - and displays popup hypertext outlining the
//properties of that element
function hoverpop(hvEl) {
  //work out where we're going to put our popup
  xpos = hvEl.offsetLeft;
  ypos = hvEl.offsetTop + hvEl.offsetHeight;
  
  
}

//hoverhide destrys the hoverpop popup
function hoverhide() {
  dbFM.objHPop.style.visibility = 'hidden';
  dbFM.objHPop = null;
}

//this can be used to hide and reveal form elements
function setVisibility(objectID, state) {
  var object = document.getElementById(objectID);
  object.style.visibility = state;
}
*/
/*
// Run upon load - takes as its parameter the id of the parent container - we can put this in the layout builder
function initPop(popParent) {
  // Set up div we will use to hover our text
  objHPop = document.createElement("div");
  // Change these to customise your popup
//  objHPop.
  objHPop.style.color = "#000000";
  objHPop.style.font = "normal xx-small verdana";
  objHPop.style.padding = "1px 1px 1px 1px";
  objHPop.style.background = "#fff588";
  objHPop.style.border = "1px solid black";
  // Don't, however, change these
  objHPop.style.left = -100;
  objHPop.style.top = -100;
  objHPop.style.position = 'absolute';
  objHPop.innerHTML = '';
  objHPop.style.zIndex = 10;

  popParent.appendChild(objHPop);
 
}

//do I really need this?? can't I just use the position of the element we're selecting??
// Keeps mouse x and y in posx and posy
function mtrack(e) {
  if (dbFM.popUp) {
    if (!e) {
      var e = window.event;
    }
    if (e.pageX || e.pageY) {
      dbFM.posx = e.pageX;
      dbFM.posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
      dbFM.posx = e.clientX + document.body.scrollLeft;
      dbFM.posy = e.clientY + document.body.scrollTop;
    }
    dbFM.objHPop.style.left = posx + offsetX;
    dbFM.objHPop.style.top = posy + offsetY;
  }
}

// Change floating div to include text on mouseover ... so what's e?
function doText(t, e) {
  dbFM.popUp = true;
  dbFM.objHPop.innerHTML = t;
}

// Change back to nothing
function doClear() {
  dbFM.popUp = false;
  dbFM.objHPop.style.left = -100;
  dbFM.objHPop.style.top = -100;
  dbFM.objHPop.innerHTML = "";
}
*/