
/* namespace */
function upFM() {}

$(window).load(upfmUploadAutoAttach);

upFM.ce = function(elementName) { elementName = elementName.toLowerCase(); var element = document.createElement(elementName); return element; }

//this can be used to hide and reveal form elements
function setVisibility(objectID,state) {
  var object = document.getElementById(objectID);
  object.style.visibility = state;
}

//Attaches the upload behaviour to the upload form.
function upfmUploadAutoAttach(){
  $('input.upfmupload').each(function () {
    var uri = this.value;
    var button = 'wfmatt-button';
    var wrapper = 'wfmatt-wrapper';
    var hide = 'wfmatt-hide';
    var upload = new upFM.jsUpload(uri, button, wrapper, hide);
  });
}

/**
 * upFM.jsUpload constructor
 *
 * 1st param is upload iframe url
 * 2nd param is id of redirected button object)
 * 3rd param is id of container of html repainted by ajax
 * 4th param is id of upload input field
 */
upFM.jsUpload = function(uri, button, wrapper, hide) {
  // Note: these elements are replaced after an upload, so we re-select them
  // everytime they are needed.
  setVisibility('progress','hidden');
  //we still need all this otherwise the upload / submit doesn't work
  this.button = '#'+ button;
  this.wrapper = '#'+ wrapper;
  this.hide = '#'+ hide;
  Drupal.redirectFormButton(uri, $(this.button).get(0), this);
}

//Handler for the form redirection submission.
upFM.jsUpload.prototype.onsubmit = function () {

  setVisibility('progress','visible');
  var hide = this.hide;
  $(hide).fadeOut('slow');
}

//Handler for the form redirection completion.
upFM.jsUpload.prototype.oncomplete = function (data) {
  parent.dbFM.dirListObj.refresh(parent.dbFM.dirListObj.content.current);
  setVisibility('progress','hidden');

  // Remove old form
  Drupal.freezeHeight(); // Avoid unnecessary scrolling
  $(this.wrapper).html('');

//do we still need to wrap this is an extra div?? WHERE do we fill in the directory??

  // Place HTML into temporary div
  var div = document.createElement('div');
  $(div).html(data['html']);
  $(this.wrapper).append(div);

  //this is where we redirect the submit button for the upload
  this.confirm = document.getElementById('replace-options');
  if(this.confirm) {
    var hide = this.hide;
    $(hide).fadeOut('slow');
    var cp = this;
    this.confirm.id = 'replace-options';
    var replaceButton = upFM.ce('input');
    replaceButton.setAttribute('type', 'button');
    replaceButton.setAttribute('value', upFM.js_msg["submit"]);
    var listener = upFM.addEventListener(replaceButton, "click", function(e) { cp.submit(); upFM.stopEvent(e); });
    this.confirm.appendChild(replaceButton);
    this.confirm.file = data['file'];
  } else {
    $(this.hide, div).fadeIn('slow');
    upFMUploadAutoAttach();
  }

  Drupal.unfreezeHeight();

  //this should work ... but it doesn't ZZZZZZZZZZZ
  //I'm confused - this would be refreshing the directory list but surely it happens before we've processed the file result??
  this.parent.upFM.dirListObj.refresh();

  //now we need to clear the greybox NOTHING BLOODY WORKS!
//    $("#GB_window,#GB_overlay").hide();
//    this.opener.$("#GB_window,#GB_overlay").hide();
//document.getElementById('GB_window').display = 'none';

//document.getElementById('GB_overlay').display = 'none';
//greything1 = self.parent.document.getElementById('GB_window');
//greything1.hide();

//greything1 = self.parent.document.getElementById('GB_window');
//greything1.css({ display: 'none' });

//greything2 = self.parent.document.getElementById('GB_overlay');
//greything2.css({ display: 'none' });

//document.GB_hide();
//self.parent.document.GB_hide();
//alert("did this rujn");
   // this.hide();
   //this.parent.GB_hide()
  //window.opener.GB_hide();
  //this.parent.GB_hide();
  
  /// I'm really having trouble here - I can't seem to run the GB_hide function
  //strange because it's not part of a document - just a top level function

}

//Handler for the form redirection error.  ZZZ does this work??
upFM.jsUpload.prototype.onerror = function (error) {
  alert('An error occurred:\n\n'+ error);
  upFM.progressObj.hide();
  if(this.confirm)
    upFM.clearNodeById('replace-options');
  // Undo hide
  $(this.hide).css({
    position: 'static',
    left: '0px'
  });
}

upFM.jsUpload.prototype.submit = function () {

  var inputs = [];
  inputs = this.confirm.getElementsByTagName('input');
  var selection = '';
  for(var i = 0; i < inputs.length; i++) {
    if(inputs[i].checked == true) {
      selection = i;
      break;
    }
  }
  if(typeof(selection) != 'undefined') {
    var url = upFM.ajaxUrl();
    upFM.progressObj.show();
    var postObj = { action:encodeURIComponent("version"), param0:encodeURIComponent(selection), param1:encodeURIComponent(this.confirm.file)};
    upFM.HTTPPost(url, this.callback, this, postObj);
  }

}

upFM.jsUpload.prototype.callback = function(string, xmlhttp, cp) {
  //Should we put a hide here?
  
  // Remove overwrite radios???
  var parent = cp.confirm.parentNode;
  parent.removeChild(parent.firstChild);
  var hide = cp.hide;
  if (xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);

    if(result.status)
      upFM.dirListObj.refresh();
    // Insert ajax feedback before upload form
    var elDiv = upFM.ce('div');
    elDiv.className = 'upload-msg';
    elDiv.appendChild(upFM.ctn(result.data));
    $(hide).before($(elDiv));

  } else {
    upFM.alrtObj.msg(upFM.js_msg["ajax-err"]);
  }


  $(hide).css({ display: 'block' });
  upFMUploadAutoAttach();

}
