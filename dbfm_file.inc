<?php

/**
 * dbfm_mkdir -called from the ajax action - switch case 'mkdir':
 * zzzzzzzzzzzz you need to update your header!!!
 * @param string $source - it's the parent directory path
 * @param string $dest - it's the proposed new directory name
 * @param bool $rename - This flag allows a directory rename for cases where the directory already exists
 *                       if you were trying to create a directory myfiles then it would try to rename it 
 *                       myfiles_01, myfiles_02 etc. until it found a name that hadn't been used
 * @param string $error - the address of an array used to store any error messages
 *
 * @ret   bool   -true if the directory is created successfully (with 775 permissions for FS) 
 */
 function dbfm_mkdir($source, $dest, $rename = FALSE, &$error) {
  //source is the parent directory, dest is the proposed name for our directory
  $mkdir_path = ($source.'/'.$dest);

  global $user;
  // we've got to work out which directory node we're adding to by searching on the fpath field
  $currentdate = date("Y-m-d H:i:s");
  $setmime = "directory";

  //we need to check first whether there's already a directory of this name, if there is then we
  // simply append a number and try again ... and again, eventually we'll find an unused name
  $count = 0;

  $numret = 1;
  $dirpath = $mkdir_path;
  while ($numret) {
    $parent = db_query("SELECT * FROM {dbfm_file} WHERE fpath = '%s'", $dirpath);
    $parentrec = db_fetch_object($parent);
    $numret = db_num_rows($parent);
    if ($numret) {
      $count++;
      $dirpath = $mkdir_path . "_" . $count;
    }
  }
  if ($count > 0) {
    $dest = $dest . "_" . $count;
  } 

  // find the database entry representing the parent
  $parent = db_query("SELECT * FROM {dbfm_file} WHERE fpath = '%s'", $source);
  $parentrec = db_fetch_object($parent);
  $numret = db_num_rows($parent);
  if ($numret != 1) {
    //should only be one parent
    $error[] = "ERROR: Unable to find parent directory " . $source;
    return FALSE;
  }
  //increment fsize - shows the number of entries in the directory
  $numdir = $parentrec->fsize + 1;

  // we'll write to the database 
  $result = db_query("INSERT INTO {dbfm_file} (uid, fpath, fname, fsize, fmime, fcreatedate, flastmod, fparent) VALUES (%d, '%s', '%s', %d, '%s', '%s', '%s', %d)",$user->uid, $dirpath, $dest, 0, $setmime, $currentdate, $currentdate, $parentrec->fid);
  if ($result === FALSE) {
    $error[] = "ERROR: Failed to insert the directory " . $dest . " into the database";
    return FALSE;
  }

  // and update the parent record - fsize stores the number of directories parented
  $result = db_query("UPDATE {dbfm_file} SET `fsize` = %d WHERE fid = %d ", $numdir, $parentrec->fid);
  if ($result === FALSE) {
    $error[] = "ERROR: Failed to update the parent directory for " . $dest;
    return FALSE;
  }
  //return the directory path of the newly created directory in the error array 
  //SUCCESS!!
  $error = $dirpath;
  return TRUE;
 }

/**
 * dbfm_rename -called from the ajax action - switch case 'rename' is this ONLY for directories??
 *
 * @param string  $source - the source directory path
 * @param string  $dest - the destination directory name
 * @param string &$err_arr - ref to error array for client feedback
 *
 * @ret   bool   -true if the directory/file is renamed and file record(s)
 *                updated if dir/file under db control
 */
function dbfm_rename($source, $dest, &$err_arr) {

  //check what the $source is - a file or directory ZZZZ will need extra code here to deal with versions
  $qsource = db_query("SELECT * FROM {dbfm_file} WHERE fpath = '%s'",$source);
  $source_rec = db_fetch_object ($qsource);
  //the source MUST already exist otherwise the user wouldn't have been able to select it

  //does the destination already exist?
  $qdest = db_query("SELECT * FROM {dbfm_file} WHERE fpath = '%s'",$dest);
  $numret = db_num_rows($qdest);
  if ($numret > 0) {
    $err_arr = "Rename failed - destination " . $dest . " already exists";
    return FALSE;
  }
  // a line to  split the filename away from the file path
  // reverse the path and look for the first slash. Get a substring of the reversed path from the beginning up to,
  //but not including, the slash. Now reverse it to give you the filename
  // hmnnn - someone not heard of strrchr?  perhaps
  //   $filename =  substr(strrchr($dest, '/'), 1);  //substr allows you to start after the slash
  $filename = strrev(substr(strrev($dest), 0, strpos(strrev($dest), '/')));

  // simplified code - we'd need to check here for a valid name and extension, things like apostrophes etc. need catching

  // i'd sure like to use a dbfm function for this but  the current update function
  // only changes path and doesn't deal with fname. For now I'll put some inline code
  // ZZZZ I think I'll update the last modified date too
  $result = db_query("UPDATE {dbfm_file} SET fpath = '%s', fname = '%s' WHERE fid = %d", $dest, $filename, $source_rec->fid);
  if($result === FALSE) {
    $err_arr = "ERROR: Rename failed. Couldn't change name to " . $filename;
    return FALSE;
  }

  if ($source_rec->fmime == 'directory' && $source_rec->fsize > 0) {
    //if it's a directory we need to recurse through the database changing the path for all the children
    //need to update the sourec record first
    $source_rec->fpath = $dest;
    $ret = TRUE;
    return dbfm_recur_dbrename($source_rec, $ret , $err_arr);
  }
  return TRUE;
  
}

/**
 * dbfm_recur_dbrename -the function to recursively rename (actually repath) database records
 *
 * @param record  $source - the source directory record
 * @param bool  $ret - the return value from the recursive function
 * @param string &$err_arr - ref to error array for client feedback
 *
 * @ret   bool   -true if the directory/file is updated in the database
 *
 */

//new function to recursively change the database fpath entries where a directory has been renamed or moved
//only used where storage is to the database
// the first parameter is a file record for the source directory (which has already had it's fpath changed to the correct one)
// the second parameter allows the return condition to be passed on, the third is the address of an array where error messages
// can be stored
function dbfm_recur_dbrename($source, $ret, &$err_arr) {
  //look in the database for children of the directory
  //for each child, re-path it. If it's a directory recurse and re-path all its children
  $qkidlist = db_query("SELECT * FROM {dbfm_file} WHERE fparent = %d", $source->fid);
  while ($sdata = db_fetch_object($qkidlist)) {
    //update the object
    $new_path = $source->fpath . "/" . $sdata->fname;
    $result = db_query("UPDATE {dbfm_file} SET fpath = '%s' WHERE fid = %d", $new_path, $sdata->fid);
    if ($result === FALSE) {
      $err_arr[] = "ERROR: Rename to " . $new_path . " for " . $sdata->fname . " failed";
      $ret = FALSE;
    }

    // if it's a directory, has it got any entries, if so recurse
    if ($sdata->fmime == 'directory' AND $sdata->fsize > 0) {
      // update the source record first 
      $sdata->fpath = $new_path;
      $ret = dbfm_recur_dbrename($sdata, $ret, $err_arr);
    }

  } // end of while loop
  return $ret;
}


/**
 * dbfm_delete - called from the ajax action - switch case 'delete':
 *
 * @param string $source - the source directory path
 * @param string &$err_arr - ref to error array for client feedback
 *
 * @ret   bool   -true if the directory or file is deleted and all file records updated
 */
function dbfm_delete($source, &$err_arr) {
  // bring back the source record from the database
  $qsource = db_query("SELECT * FROM {dbfm_file} WHERE fpath = '%s'", $source);
  $numret = db_num_rows($qsource);
  if ($numret != 1) {
    //either no records or too many records have been found
    $err_arr[] = "ERROR: Unable to recover the database entry for " . $source;
    return FALSE; 
  }
  $rec_to_delete = db_fetch_object($qsource);

  // in fact there's a sneaky way of dealing with the whole thing... we'll just mark the parent record id as -1
  // fill in the last modified date. then have an admin function that REALLY deletes these entries at a later date
  // this means we don't need a recursive directory delete (at least not here, it will be in admin). 
  // Directories and their contents become invisible
  $currentdate = date("Y-m-d H:i:s");

  //need to update the parent record
  $qparent = db_query("SELECT * FROM {dbfm_file} WHERE fid = %d", $rec_to_delete->parentid);
  $parentrec = db_fetch_object($qsource);
  $newfilecount = $parentrec->fsize - 1;
  $result = db_query("UPDATE {dbfm_file} SET fsize = %d, flastmod = '%s' WHERE fid = %d", $newfilecount, $currentdate, $parentrec->fid);
  if ($result === FALSE) {
    $err_arr[] = "ERROR: Unable to update filecount for directory " . $parentrec->fname;
    return FALSE;
  }

  $result = db_query("UPDATE {dbfm_file} SET fparent = %d, flastmod = '%s' WHERE fid = %d", '-1', $currentdate, $rec_to_delete->fid);
  if ($result === FALSE) {
    $err_arr[] = "ERROR: Delete Failed for " . $rec_to_delete->fname;
    return FALSE;
  }
  return TRUE;
}


/**
 * Move a directory or file and update database
 *
 * @param string  $source - the source directory path (what you're moving)
 * @param string  $dest - the destination directory name (where you're moving it)
 * @param string  &$err_arr - ref to error array for client feedback
 *
 * @ret   bool   -true if the file and the database entries are updated successfully
 *
 */
function dbfm_move($source, $dest, &$err_arr) {

  //read the db record for that path
  $qsource = db_query("SELECT * FROM {dbfm_file} WHERE fpath = '%s'",$source);
  $source_rec = db_fetch_object($qsource);
  //the source MUST already exist otherwise the user wouldn't have been able to select it

  //also open the $source files current parent - we need to strip back to the last slash
  $parentpath =  substr($source, 0, strrpos($source, '/'));

  $qoldparent = db_query("SELECT * FROM {dbfm_file} WHERE fpath = '%s'",$parentpath);
  $numret = db_num_rows($qoldparent);
  if ($numret != 1) {
    $err_arr[] = "Move failed - unable to open parent directory " . $parentpath;
    return FALSE;
  }
  $parent_rec = db_fetch_object($qoldparent);

  //does the destination directory already exist - it should do - error if not!
  $qdest = db_query("SELECT * FROM {dbfm_file} WHERE fpath = '%s'",$dest);
  $numret = db_num_rows($qdest);
  if ($numret != 1) {
    // this can't happen! the record must exist for it to have been displayed on screen
    $err_arr[] = "Move failed - destination " . $dest . " does not exist";
    return FALSE;
  }

  // check the destination is a directory and not a file
  $dest_rec = db_fetch_object ($qdest);
  if ($dest_rec->fmime != 'directory') {
    $err_arr[] = "Move failed - destination " . $dest . " must be a directory";
    return FALSE;
  }

  $new_path = $dest . '/' . $source_rec->fname;

  // we first check whether a file / directory of that name already exists in the destination 
  // directory. (that's if the directory has any entries)
  if ($dest_rec->fsize > 0) {
    $qdestkids = db_query("SELECT * FROM {dbfm_file} WHERE fparent = %d", $dest_rec->fid);
    //look through the children in the destination directory - if one has the same name then
    while ($dest_kidrec = db_fetch_object ($qdestkids)) {
      if ($dest_kidrec-> fpath == $new_path) {
        $err_arr[] = "ERROR: An object of that name already exists in the destination directory";
        return FALSE;
      }
    }
  }
  
  //edit the curent record to correct the parent and change the path
  $result = db_query("UPDATE {dbfm_file} SET fpath = '%s', fparent = %d WHERE fid = %d", $new_path, $dest_rec->fid, $source_rec->fid);
  if ($result === FALSE) {
    $err_arr[] = "ERROR: Unable to update the database path for move to " . $new_path;
    return FALSE;
  }

  //edit the destination parent record to add one to the number of entries held in the fsize field
  $result = db_query("UPDATE {dbfm_file} SET fsize = %d WHERE fid = %d", $dest_rec->fsize + 1, $dest_rec->fid);
  if ($result === FALSE) {
    $err_arr[] = "ERROR: Unable to update the destination parent directory " . $dest_rec->fpath;
    return FALSE;
  }

  //edit the previous parent record to deduct one from the number of entries held in the fsize field
  $result = db_query("UPDATE {dbfm_file} SET fsize = %d WHERE fid = %d", $parent_rec->fsize - 1, $parent_rec->fid);
  if ($result === FALSE) {
    $err_arr[] = "ERROR: Unable to update the previous parent directory " . $parentpath;
    return FALSE;
  }

  $ret = TRUE;

  //test whether the source is a directory, if so we need to recursively change all the children of the directory
  if ($source_rec->fmime == 'directory' && $source_rec->fsize > 0) {
    //we need to update the source record first 
    $source_rec->fpath = $new_path;
    $ret = dbfm_recur_dbrename($source_rec, $ret, $err_arr);
  }
  return $ret;
}


/**
 * dbfm_insert_file - inserts a file into the dbfm_file table
 *
 * @param string  $path - path relative to drupal root
 * @param string &$err_arr - ref to error array for client feedback
 *
 * @return bool - TRUE if query executed successfully, otherwise FALSE
 */
function dbfm_insert_file($path, &$err_arr){
  $file = new stdClass();
  $file->filepath = $path;
  if(dbfm_enum_validate($file, $err_arr)) {
    if(($ret = dbfm_dbinsert_file($file, $err)) === FALSE) {
      $err_arr[] = $err;
    }
    return $ret;
  }
  return FALSE;
}

?>
